from settings import *

def main():
    connectToItemDB()
    
    values = getValues( 'start_items!A1' )

    print('total items = %s\n' % values[0][0] )
    
    itemCount = int( values[ 0 ][ 0 ] )
    
    values = getValues( 'start_items!A3:AG%s' % ( itemCount + 2 ) )
    
    f = open("../mod/common/scripted_effects/auto_gen_unique_items.txt","w")
    
    addLine( f, 'init_unique_items = {' )
    
    t = 0
    for row in values:
        if len( row ) < 33:
            row = row + ( [''] * ( 33 - len( row ) ) )
            
        if row[ 2 ] == '':
            continue
            
        t = t + 1
        
        print('%s: %s' % ( str(t+2), row[2]))
        
        addLine( f, '#' + str(row[ 2 ].encode('ascii',errors='ignore')) )
        addLine( f, 'create_empty_item = yes' )
        addLine( f, 'set_variable = { global.item_types^n = %s }' , row[ 6 ] )
        addLine( f, 'set_variable = { global.item_levels^n = %s }' , row[ 5 ] )
        addLine( f, 'set_variable = { global.item_qualities^n = %s }' , row[ 8 ] )
        addLine( f, 'set_variable = { global.item_indices^n = %s }' , row[ 7 ] )
        addLine( f, 'set_variable = { global.icon_var^n = %s }' , ( int( row[ 6 ] ) * 10 + int( (int( row[ 7 ] )-1) / 16) ) )
        addLine( f, 'set_variable = { global.item_unique_name^n = %s }' , t )
        addLine( f, 'set_variable = { global.icon_frames^n = %s }' , ( (int( row[ 7 ] )-1) % 16 + 1 ) )
        addLine( f, 'set_variable = { global.item_u_stat^n = %s }' , row[ 9 ] )
        addLine( f, 'set_variable = { global.item_u_stat_a^n = %s }' , row[ 10 ] )
        addLine( f, 'set_variable = { global.item_u_stat_b^n = %s }' , row[ 11 ] )
        addLine( f, 'set_variable = { global.item_u_stat_c^n = %s }' , row[ 12 ] )
        addLine( f, 'set_variable = { global.item_stat_1^n = %s }' , row[ 13 ] )
        addLine( f, 'set_variable = { global.item_stat_1_a^n = %s }' , row[ 14 ] )
        addLine( f, 'set_variable = { global.item_stat_1_b^n = %s }' , row[ 15 ] )
        addLine( f, 'set_variable = { global.item_stat_1_c^n = %s }' , row[ 16 ] )
        addLine( f, 'set_variable = { global.item_stat_2^n = %s }' , row[ 17 ] )
        addLine( f, 'set_variable = { global.item_stat_2_a^n = %s }' , row[ 18 ] )
        addLine( f, 'set_variable = { global.item_stat_2_b^n = %s }' , row[ 19 ] )
        addLine( f, 'set_variable = { global.item_stat_2_c^n = %s }' , row[ 20 ] )
        addLine( f, 'set_variable = { global.item_stat_3^n = %s }' , row[ 21 ] )
        addLine( f, 'set_variable = { global.item_stat_3_a^n = %s }' , row[ 22 ] )
        addLine( f, 'set_variable = { global.item_stat_3_b^n = %s }' , row[ 23 ] )
        addLine( f, 'set_variable = { global.item_stat_3_c^n = %s }' , row[ 24 ] )
        addLine( f, 'set_variable = { global.item_stat_4^n = %s }' , row[ 25 ] )
        addLine( f, 'set_variable = { global.item_stat_4_a^n = %s }' , row[ 26 ] )
        addLine( f, 'set_variable = { global.item_stat_4_b^n = %s }' , row[ 27 ] )
        addLine( f, 'set_variable = { global.item_stat_4_c^n = %s }' , row[ 28 ] )
        addLine( f, 'set_variable = { global.item_stat_5^n = %s }' , row[ 29 ] )
        addLine( f, 'set_variable = { global.item_stat_5_a^n = %s }' , row[ 30 ] )
        addLine( f, 'set_variable = { global.item_stat_5_b^n = %s }' , row[ 31 ] )
        addLine( f, 'set_variable = { global.item_stat_5_c^n = %s }' , row[ 32 ] )
        addLine( f, "add_randomized_stats = yes")

        if int( row[ 5 ] ) == -1:
            addLine( f, """
set_temp_variable = { leveling_index = n }
create_empty_item = yes
init_leveling_item = yes
""" )
        
        addLine( f, "set_temp_variable = { item_build_level = 1 }")
        addLine( f, "build_leveling_item = yes")

        if row[0] != '':
            if row[1] == '-1':
                s = "add_last_item_to_inventory = yes"
            else:
                s = "set_variable = { inventory^%s = n }" % row[1]
            addLine( f, """
%s = {
    build_item_stats = yes
    %s
    init_unique_item = yes
}""" % (row[0], s))
          
    addLine( f, '}' )
    
    
    t = 0
    f = open("../mod/common/scripted_localisation/auto_gen_unique_names.txt","w")
    
    addLine( f, """defined_text = { 
    name = ITEM_UNIQUE_NAME""" )
    
    for row in values:
        if len( row ) < 33:
            row = row + ( [''] * ( 33 - len( row ) ) )
            
        if row[ 2 ] == '':
            continue
            
        t = t + 1
        
        addLine( f, """\
    text = {
        trigger = {
            check_variable = { global.item_unique_name^v = %s }
        }
        localization_key = "%s"
    }""" % ( str( t ), row[ 2 ] ) )
    
    addLine( f, """\
    text = {
        localization_key = "invalid"
    }
}""" )
    
    t = 0
    f = open("../mod/common/scripted_localisation/auto_gen_unique_desc.txt","w")
    
    addLine( f, """defined_text = { 
    name = ITEM_UNIQUE_DESC""" )
    
    for row in values:
        if len( row ) < 33:
            row = row + ( [''] * ( 33 - len( row ) ) )
            
        if row[ 2 ] == '':
            continue
            
        t = t + 1
        
        if row[ 3 ] == '':
            continue
            
        addLine( f, """\
    text = {
        trigger = {
            check_variable = { global.item_unique_name^v = %s }
        }
        localization_key = "%s"
    }""" % ( str( t ), row[ 3 ] ) )
    
    addLine( f, """\
    text = {
        localization_key = ""
    }
}""" )

if __name__ == '__main__':
    main()

