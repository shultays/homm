from settings import *

def build(type):

    values = getValues( 'items!A1' )

    print('%s total stat = %s\n' % ( type, values[0][0] ) )
    
    statCount = int( values[ 0 ][ 0 ] ) - 1 
    
    values = getValues( 'pre_post_fixes!D2:M%s' % ( str( statCount * 2 + 1 ) ) )
    
    f = open("../mod/common/scripted_effects/auto_gen_%s_stat_pre_post_counts.txt" % type,"w")
    
    addLine( f, """\
init_%s_stat_counts = {
clear_array = global.%s_stat_pre_counts
clear_array = global.%s_stat_post_counts
add_to_array = { global.%s_stat_pre_counts = 0}
add_to_array = { global.%s_stat_post_counts = 0}""" % (type, type, type, type, type) )
    
    t = 0
    for a in range( 0, statCount * 2, 2 ):
        row_pre = values[ a ]
        row_post = values[ a + 1 ]
        
        t += 1
        
        print( t )
        
        addLine( f, "add_to_array = { global.%s_stat_pre_counts = %s }" % (type, len( row_pre )) )
        addLine( f, "add_to_array = { global.%s_stat_post_counts = %s }" % (type, len( row_post )) )
        
    
    addLine( f, "}" )
    
     
    f = open("../mod/common/scripted_localisation/auto_gen_%s_pre_post.txt" % type,"w")
    
    addLine( f, """\
defined_text = { 
    name = homm_%s_prefix
    text = {
        trigger = {
                check_variable = { global.item_name_pre^v = 0 }
        }
        localization_key = ""
    }""" % (type))
    
    t = 0
    for a in range( 0, statCount * 2, 2 ):
        
        t += 1
        
        print( t )
        
        addLine( f, """\
    text = {
        trigger = {
                check_variable = { global.item_name_pre^v  < %s }
        }
        localization_key = "[homm_%s_prefix_%s]"
    }""" % ( str( ( t + 1 ) * 10 ), type, str( t * 10 ) ) )
    
    
    addLine( f, """\
    text = {
        localization_key = "invalid"
    }
}""" )


    t = 0
    for a in range( 0, statCount * 2, 2 ):
        
        t += 1
        
        print( t )
            
        addLine( f, """\
defined_text = { 
    name = homm_%s_prefix_%s""" % (type, str( t * 10 )) )
        
        row = values[ a ]
        
        k = 0
        for s in row:
            addLine( f, """\
    text = {
        trigger = {
                check_variable = { global.item_name_pre^v  = %s }
        }
        localization_key = "%s "
    }""" % ( str( t * 10 + k ), s ) )
            k += 1
    
    
        addLine( f, """\
    text = {
        localization_key = "Invalid"
    }
}""" )


    addLine( f, """\
defined_text = { 
    name = homm_%s_postfix
    text = {
        trigger = {
                check_variable = { global.item_name_post^v = 0 }
        }
        localization_key = ""
    }""" % (type))
    
    t = 0
    for a in range( 0, statCount * 2, 2 ):
        
        t += 1
        
        print( t )
        
        addLine( f, """\
    text = {
        trigger = {
                check_variable = { global.item_name_post^v  < %s }
        }
        localization_key = "[homm_%s_postfix_%s]"
    }""" % ( str( ( t + 1 ) * 10 ), type, str( t * 10 ) ) )
    
    
    addLine( f, """\
    text = {
        localization_key = "invalid"
    }
}""" )


    t = 0
    for a in range( 0, statCount * 2, 2 ):
        
        t += 1
        
        print( t )
            
        addLine( f, """\
defined_text = { 
    name = homm_%s_postfix_%s""" % (type, str( t * 10 )) )
        
        row = values[ a + 1 ]
        
        k = 0
        for s in row:
            addLine( f, """\
    text = {
        trigger = {
                check_variable = { global.item_name_post^v  = %s }
        }
        localization_key = "%s"
    }""" % ( str( t * 10 + k ), " of " + s ) )
            k += 1
    
    
        addLine( f, """\
    text = {
        localization_key = "Invalid"
    }
}""" )

def main():
    connectToItemDB()
    
    build("item")
    
    connectToArtifactDB()
    
    build("artifact")


    
if __name__ == '__main__':
    main()

