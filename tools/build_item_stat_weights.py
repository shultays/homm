from settings import *

def BuildItemStatWeights( sh, f ):
    print( sh )
    
    values = getValues( '%s!A1:B1' % sh )

    print('total items = %s %s\n' % ( values[0][0], values[0][1] ) )
    
    statCount = int( values[ 0 ][ 0 ] )
    typeCount = int( values[ 0 ][ 1 ] )
    
    addLine( f, "# %s" % sh )
    
    values = getValues( '%s!A:A' % sh )
    
    
    customStats = {}
    
    statStart = 1
    while statStart < len(values):
        if len(values[statStart]) > 0:
            s = str(values[statStart][0])
            if s == '0':
                break
                
            if not s.isdigit():
                customStats[statStart] = s
        statStart += 1
    
    values = getValues( '%s!D1:%s%s' % ( sh, columnString(typeCount + 4), statCount + 3 ) )
        
    t = 0
    for row in values:
        if len( row ) < typeCount:
            row = row + ( [''] * ( typeCount - len( row ) ) )
        
    for i in range( typeCount ):
        addLine( f, "# %s" % values[1][i] )
        
        type_s = values[1][i].replace(" ", "_").lower()
        addLine( f, """\
%s = {
set_variable = { global.%s_scope = this }
clear_array = stat_chance
resize_array = { array = stat_chance size = %s }
set_variable = { stat_chance^0 = %s }""" % (values[0][i], type_s, statCount, values[statStart][i] ) )
    #    for j in range( statCount ):
    
        for j in range( 1, statCount ):
            if len( values ) > j + statStart and len(values[j+statStart]) > i and values[j+statStart][i] != '':
                addLine( f, "set_variable = { stat_chance^%s = %s }" % ( j, values[j+statStart][i] ) )
        
        for j in customStats:
            if len( values ) > j + statStart and len(values[j]) > i and values[j][i] != '':
                addLine( f, "set_variable = { %s = %s }" % ( customStats[j], values[j][i] ) )
            
            
        addLine( f, "}")
    
    
def main():
    connectToItemDB()
    
    f = open("../mod/common/scripted_effects/auto_gen_item_stat_weights.txt","w")
    
    addLine( f, "init_item_stat_weights = {" )
    
    BuildItemStatWeights( "sword", f )
    BuildItemStatWeights( "armor", f )
    BuildItemStatWeights( "helm", f )
    BuildItemStatWeights( "cloak", f )
    BuildItemStatWeights( "shield", f )
    BuildItemStatWeights( "ring", f )
    BuildItemStatWeights( "amulet", f )
    BuildItemStatWeights( "boots", f )
    BuildItemStatWeights( "gloves", f )
    BuildItemStatWeights( "axe", f )
    BuildItemStatWeights( "spear", f )
    BuildItemStatWeights( "mace", f )
    
    
    connectToArtifactDB()
    
    BuildItemStatWeights( "artifact", f )
    
    addLine( f, "}" )
    
if __name__ == '__main__':
    main()

