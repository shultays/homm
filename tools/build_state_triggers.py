from settings import *

def main():
    f = open("../mod/common/scripted_triggers/auto_gen_state_triggers.txt","w")
    
    for i in range(1, 1000):
        addLine( f, "monster_mesh_visible_%s = { controls_state = %s }" % ((str(i), str(i))))

if __name__ == '__main__':
    main()

