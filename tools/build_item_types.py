from settings import *

def main():
    item_types = {}
    sorted_types = []

    connectToItemDB()
    
    values = getValues( 'item_types!B:B' )

    row = 0
    for t in values:
        row += 1
        if not t:
            continue
        item_type = t[0]
        
        if not item_type in item_types:
            item_types[item_type] = []
            sorted_types.append(item_type)
          
        arr = item_types[item_type]
        
        values = getValues( 'item_types!C%s:R%s' % (row, row) )[0]
        for type in values:
            if type:
                arr.append(type)
        
    f = open("../mod/common/scripted_effects/auto_gen_item_types.txt","w")
    
    addLine( f, 'init_all_item_types = {')
    for item_type in sorted_types:
        addLine( f, 'init_%s_types = yes' % item_type )

    addLine( f, '}' )

    for item_type in sorted_types:
        addLine( f, '''
init_%s_types = {
clear_array = global.%s_types''' % (item_type, item_type))
        for t in item_types[item_type]:
            addLine( f, 'add_to_array = { global.%s_types = %s }' % (item_type, t))
        addLine( f, '}')

    
if __name__ == '__main__':
    main()

