
SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly']

ACTOR_SHEET = '1xSH6BHPEt9B2_P-yAJcvPMddPNpjCC228qz8xLN1LHQ'
ITEM_SHEET = '1HZzHnN9D3HiAVmRvcOCnL1PT9_Kkzk1jb_Rlc4KAFpQ'
ARTIFACT_SHEET = '17iCIumGvZVmSZhU8vEGiHCyAqyqKfmzDdKsr-y6ffkk'

currentSheet = ACTOR_SHEET

import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

def addLine( f, str, val1 = 10000 ):
    if val1 == 10000:
        f.write( str + '\n' )
    elif val1 != '':
        f.write( str % val1 + '\n' )

def columnString(n):
    string = ""
    while n > 0:
        n, remainder = divmod(n - 1, 26)
        string = chr(65 + remainder) + string
    return string

service = 0
sheet = 0

def connectToActorDB():
    global currentSheet
    currentSheet = ACTOR_SHEET
    startService()
    
def connectToItemDB():
    global currentSheet
    currentSheet = ITEM_SHEET
    startService()

def connectToArtifactDB():
    global currentSheet
    currentSheet = ARTIFACT_SHEET
    startService()
    
def startService():
    global sheet
    global service
    global currentSheet
    
    creds = None
    
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
            
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server()
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('sheets', 'v4', credentials=creds)

    sheet = service.spreadsheets()
    
def getValues( table ):
    global sheet
    global service
    global currentSheet
    
    result = sheet.values().get(spreadsheetId=currentSheet,
                                range=( table ) ).execute()
    values =  result.get('values', [])
    return values