from settings import *

def build(type):
    values = getValues( 'items!A1' )

    print('%s total stat = %s\n' % ( type, values[0][0] ) )
    
    statCount = int( values[ 0 ][ 0 ] ) - 1 
    
    values = getValues( 'items!B3:B%s' % ( str( statCount + 2 ) ) )
    
    f = open("../mod/common/scripted_effects/auto_gen_%s_stat_build.txt" % type,"w")
    addLine( f, "build_%s_stat = {" % type )
    
    t = 0
    for row in values:
        
        t += 1
        
        addLine( f, """\
if = {
    limit = { check_variable = { t_i = %s } }
    build_stat_%s_%s = yes
}""" % ( str( t ), str( t ), row[0] ) )
    
    addLine( f, "}" )
    
    f = open("../mod/common/scripted_effects/auto_gen_%s_add_actor_stats.txt" % type,"w")
    addLine( f, "add_%s_stat = {" % type )
    
    t = 0
    for row in values:
        
        t += 1
        
        print( t, ": ", row[ 0 ] )
        
        addLine( f, """\
if = {
    limit = { check_variable = { t_i = %s } }
    add_equipment_stat_%s_%s = yes
}""" % ( str( t ), str( t ), row[0] ) )
    
    addLine( f, "}" )
    
    values = getValues( 'descbuilder!C7:C%s' % ( str( statCount + 6 ) ) )
    
    f = open("../mod/common/scripted_localisation/auto_gen_%s_stat_desc.txt" % type,"w")
    
    for k in range( 1, 6 ):
        t = 0
        addLine( f, """\
defined_text = { 
    name = homm_%s_desc_%s""" % (type, str( k ))
        )
        for row in values:
            
            t += 1
            
            print( t, ": ", row[ 0 ] )
            
            addLine( f, """\
    text = {
        trigger = {
            check_variable = { global.item_stat_%s^v = %s }
        }
        localization_key = "%s"
    }""" % ( str( k ), str( t ), row[ 0 ].replace("[x]", str( k ) ) ) )
        
    
        addLine( f, "}" )
        
def main():
    connectToItemDB()
    
    build("item")
    
    connectToArtifactDB()
    
    build("artifact")
    
    
if __name__ == '__main__':
    main()

