from settings import *


def main():
    connectToActorDB()
    
    values = getValues('stats!A1:B1') 

    print('total stat = %s %s\n' % ( values[0][0], values[0][1] ) )
    
    statCount = int( values[ 0 ][ 0 ] )
    templateCount = int( values[ 0 ][ 1 ] )
    
    
    stats = getValues( 'stats!A3:B%s' % ( str( statCount + 2 ) ) )
    
    templates = getValues( 'stats!C2:%s%s' % ( columnString( templateCount + 2 ), str( statCount + 2 ) ) )
    
    
    values = getValues( 'actor_effects!A1' )
    
    actorStatCount = int( values[ 0 ][ 0 ] )
    print( actorStatCount )
    
    
    actorStats = getValues( 'actor_effects!A7:B%s' % ( str( actorStatCount + 6 ) ) )
    
    print( actorStats )
    
    f = open("../mod/common/scripted_effects/auto_gen_actor_template_effects.txt","w")
    
    addLine(f, "init_actor_templates = {")
    
    for row in stats:
        addLine( f, "clear_array = global.actor_template_base_%s" % row[ 0 ] )
    
    for i in range( templateCount ):
        
        addLine( f, "# %s" % templates[ 0 ][ i ] )
        t = 0
        for row in stats:
            addLine( f, "add_to_array = { global.actor_template_base_%s = %s }" % ( row[ 0 ], templates[ t + 1 ][ i ] ) )
            t += 1
    
    addLine(f, "}")
    
    
    allStats = actorStats[:]
    templateInActorStats = []
    for row in stats:
        if len( row ) < 2 or "t" not in row[ 1 ]:
            allStats += [ [ "actor_stats_" + row[ 0 ] ] ]
            templateInActorStats +=  [ row[ 0 ] ]
            
    
    f = open("../mod/common/scripted_effects/auto_gen_actor_effects.txt","w")
    
    addLine( f, "create_new_actor = {" )
    for row in allStats:
        addLine( f, "add_to_array = { %s = 0 }" % row[ 0 ] )
    addLine( f, "}" )
    
    addLine( f, "remove_actor = {" )
    for row in allStats:
        addLine( f, "remove_from_array = { array = %s index = a }" % row[ 0 ] )
    addLine( f, "}" )
    
    addLine( f, "swap_actors = {" )
    for row in allStats:
        addLine( f, "set_variable = { swap_t = %s^a }" % row[ 0 ] )
        addLine( f, "set_variable = { %s^a = %s^b }" % ( row[ 0 ], row[ 0 ] ) )
        addLine( f, "set_variable = { %s^b = swap_t }" % row[ 0 ] )
    addLine( f, "}" )
    
    addLine( f, "copy_actor_from_other = {" )
    for row in allStats:
        addLine( f, "add_to_array = { %s = other:%s^a }" % ( row[ 0 ], row[ 0 ] ) )
    addLine( f, "}" )
    
    addLine( f, """
set_actor_to_template = {
set_temp_variable = { e = actor_template^a }""" )
    for row in templateInActorStats:
        addLine( f, "set_variable = { actor_stats_%s^a = global.actor_template_base_%s^e }" % ( row, row ) )
    addLine( f, "}" )
    
    addLine( f, "clear_actors = {" )
    for row in allStats:
        addLine( f, "clear_array = %s" % row[ 0 ] )
    addLine( f, "}" )
        
    addLine( f, """\
set_actor_to_template_weighted = {
set_temp_variable = { e_1 = actor_template^a }
set_temp_variable = { e_2 = actor_template_2^a }
set_temp_variable = { e_1_w = 1 }
subtract_from_temp_variable = { e_1_w = actor_template_2_weight^a }
set_temp_variable = { e_2_w = actor_template_2_weight^a }""" )
    for row in templateInActorStats:
        addLine( f, """\
if = {
    limit = { check_variable = { global.actor_template_base_[x]^e_2 > 0 } }

    set_temp_variable = { t_1 = global.actor_template_base_[x]^e_1 }
    multiply_temp_variable = { t_1 = e_1_w }
    set_temp_variable = { t_2 = global.actor_template_base_[x]^e_2 }
    multiply_temp_variable = { t_2 = e_2_w }
    set_variable = { actor_stats_[x]^a = t_1 }
    add_to_variable = { actor_stats_[x]^a = t_2 }
}
else = {
    set_variable = { actor_stats_[x]^a = global.actor_template_base_[x]^e_1 }
}""".replace("[x]", row ) )
    addLine( f, "}" )
    
    addLine( f, "set_actor_to_template_weighted_post = {" )
    for row in stats:
        if len( row ) >= 2 and "t" not in row[ 1 ] and "i" in row[ 1 ]:
            addLine( f, "round_variable = actor_stats_[x]^a".replace("[x]", row[ 0 ] ) )
    addLine( f, "}" )
    
    f = open("../mod/common/scripted_localisation/auto_gen_actor_scripted_loc.txt","w")
    
    addLine(f, """\
defined_text = { 
    name = ACTOR_DEBUG_INFO
    text = {""" )
    
    s = "        localization_key = \""
    for row in allStats:
        ss = row[ 0 ].replace( "actor_", "a_" )
        ss = ss.replace( "stats_", "s_" )
        s += "%s=[?%s^a][BR]" % ( ss, row[ 0 ] )
        
    addLine(f, s + "\"" )
    addLine(f, "    }")
    addLine(f, "}")
    
    
if __name__ == '__main__':
    main()

