from settings import *


def main():
    connectToArtifactDB()
    
    values = getValues( 'artifacts!A1' )
    
    print('total stat = %s\n' %  values[0][0] )
    
    statCount = int( values[ 0 ][ 0 ] ) - 1 
    
    values = getValues( 'artifacts!B3:U%s' % ( str( statCount + 2 ) ) )
    
    f = open("../mod/common/scripted_effects/auto_gen_artifact_effects.txt","w")
    
    addLine( f, "clear_artifact_vars = {" )
    
    columns = [2, 8, 14]
    t = 0
    for row in values:
        t += 1
        
        for c in columns:
            if len(row) > c:
                addLine( f, "clear_variable = ar_%s" % row[c])
        
    addLine( f, "}\n" )
        
    t = 0
    for row in values:
        t += 1
        
        addLine( f, "# %s" % (row[0]))
        addLine( f, "add_equipment_stat_%s_%s = {" % (t, row[0]))
        
        k = 0
        for c in columns:
            if len(row) > c:
                addLine( f, "add_to_variable = { ar_%s = t_%s }"  % (row[c], chr(97 + k)))
            k += 1
                
        addLine( f, "}\n" )
                
        addLine( f, "build_stat_%s_%s = {" % (t, row[0]))
        
        addLine( f, """
set_temp_variable = { t2 = item_level }
divide_temp_variable = { t2 = 100 }
""")

        k = 0
        for c in columns:
        
            if len(row) > c:
                a = float(row[c+1])  #0.1
                a1 = float(row[c+2])-a #0.15-0.1
                
                b = float(row[c+3]) - float(row[c+1]) #0.25
                b1 = float(row[c+4])- float(row[c+2]) - b#0.4-0.25
                
                a = "{:0.3f}".format(a)
                a1 = "{:0.3f}".format(a1)
                b = "{:0.3f}".format(b)
                b1 = "{:0.3f}".format(b1)
                
                addLine( f, "#%s,%s => %s,%s" % (row[c+1], row[c+2], row[c+3], row[c+4]))
                
                addLine( f, """
set_temp_variable = { t3 = %s }
multiply_temp_variable = { t3 = t2 }
add_to_temp_variable = { t3 = %s }
""" % (a1, a))

                addLine( f, """
set_temp_variable = { t4 = %s }
multiply_temp_variable = { t4 = t2 }
add_to_temp_variable = { t4 = %s }
""" % (b1, b))
                
                #t3 -> t3+ t4
                
                addLine( f, """
get_variance = yes
multiply_temp_variable = { t = t4 }
add_to_temp_variable = { t = t3 }
""" )

                if row[c-1].find("r") > -1:
                    addLine( f, "round_temp_variable = t")
                    
                addLine( f, "set_temp_variable = { t_%s = t }" %(chr(97 + k)))

            k += 1
                
        addLine( f, "}\n" )
                
    f = open("../mod/common/scripted_localisation/auto_gen_modifier_loc.txt","w")
    f2 = open("../mod/common/dynamic_modifiers/auto_gen_artifact_modifier.txt","w")
    
    mapped = {}
    mapped["experience_gain_army"] = "MODIFIER_XP_GAIN_ARMY"
    mapped["experience_gain_navy"] = "MODIFIER_XP_GAIN_NAVY"
    mapped["experience_gain_air"] = "MODIFIER_XP_GAIN_AIR"
    mapped["compliance_gain"] = "MODIFIER_COMPLIANCE_GAIN_ADD"
    mapped["resistance_damage_to_garrison"] = "MODIFIER_RESISTANCE_DAMAGE_TO_GARRISONS"
    
    mapped["army_attack_factor"] = "MODIFIERS_ARMY_ATTACK_FACTOR"
    mapped["army_defence_factor"] = "MODIFIERS_ARMY_DEFENCE_FACTOR"
    mapped["army_core_attack_factor"] = "MODIFIERS_ARMY_CORE_ATTACK_FACTOR"
    mapped["army_core_defence_factor"] = "MODIFIERS_ARMY_CORE_DEFENCE_FACTOR"
    mapped["army_infantry_attack_factor"] = "MODIFIERS_ARMY_INFANTRY_ATTACK_FACTOR"
    mapped["army_infantry_defence_factor"] = "MODIFIERS_ARMY_INFANTRY_DEFENCE_FACTOR"
    mapped["army_armor_attack_factor"] = "MODIFIERS_ARMY_ARMOR_ATTACK_FACTOR"
    mapped["army_armor_defence_factor"] = "MODIFIERS_ARMY_ARMOR_DEFENCE_FACTOR"
    mapped["army_artillery_attack_factor"] = "MODIFIERS_ARMY_ARTILLERY_ATTACK_FACTOR"
    mapped["army_artillery_defence_factor"] = "MODIFIERS_ARMY_ARTILLERY_DEFENCE_FACTOR"
    
    mapped["breakthrough_factor"] = "MODIFIER_BREAKTHROUGH"
 
    mapped["paratrooper_aa_defense"] = "MODIFIER_PARATROOPER_DEFENSE"
    mapped["paratrooper_count_per_plane"] = "MODIFIER_UNIT_SIZE_FACTOR_FOR_PARADROP"
    
    mapped["industrial_capacity_factory"] = "MODIFIER_INDUSTRIAL_CAPACITY_FACTOR"
    mapped["industrial_capacity_dockyard"] = "MODIFIER_INDUSTRIAL_CAPACITY_DOCKYARD_FACTOR"
    
    mapped["naval_invasion_prep_speed"] = "MODIFIER_NAVAL_INVASION_PLANNING_SPEED"
    
    added = set()
    
    addLine(f2, "homm_artifact_modifier = {")
    
    t = 0
    for row in values:
        t += 1
 
        k = 0
        for c in columns:
            if len(row) > c and not row[c] in added:
                added.add(row[c])
                lower = row[c-1].find("m") > -1
                s = "MODIFIER_" + row[c].upper()
                if lower:
                    s = s.lower()
                if row[c] in mapped:
                    s = mapped[row[c]]
                addLine(f, """\
defined_text = { 
    name = mod_%s
    text = {
        localization_key = %s
    }
}
""" % (row[c], s) )
                addLine(f2, "%s = ar_%s" % (row[c], row[c]))
    
    
    addLine(f2, "}")
    
if __name__ == '__main__':
    main()

