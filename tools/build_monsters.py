from settings import *

def main():
    connectToActorDB()
    
    values = getValues( 'monsters!A1' )

    print('total = %s\n' % ( values[0][0] ) )
    
    monsterCount = int( values[ 0 ][ 0 ] )
    
    values = getValues( 'monsters!A3:C%s' % ( str( monsterCount + 2 ) ) )
    
    f = open("../mod/common/scripted_effects/auto_gen_monster_effects.txt","w")
    
    addLine(f, "init_monster_spawns = {")
    addLine(f, "clear_array = global.monster_spawn_t1")
    addLine(f, "clear_array = global.monster_spawn_t2")
    for row in values:
        addLine(f, "# %s " % row[ 0 ] )
        addLine(f, "add_to_array = { global.monster_spawn_t1 = %s }" % row[ 1 ] )
        addLine(f, "add_to_array = { global.monster_spawn_t2 = %s }" % row[ 2 ] )
    addLine(f, "}")
    
    
    values = getValues( 'names!A1' )
    
    nameCount = int( values[ 0 ][ 0 ] )
    
    values = getValues( 'names!A3:Z%s' % ( str( nameCount + 2 ) ) )
    
    
    f = open("../mod/common/scripted_localisation/auto_gen_monster_names.txt","w")
    
    addLine( f, """defined_text = { 
    name = MONSTER_NAME
    text = {
        trigger = {
            check_variable = { actor_template^a = 1 }
        }
        localization_key = "[this.GetLeader]"
    }""" )
    
    
    for row in values:
        count = int( row[ 1 ] )
        start = int( row[ 0 ] ) * 100
        for i in range( count ):
            
            addLine( f, """\
    text = {
        trigger = {
            check_variable = { actor_name^a = %s }
        }
        localization_key = "%s"
    }""" % ( str( i + start ), row[ 2 + i ] ) )
    
    addLine( f, """\
    text = {
        localization_key = "a=[?actor_name^a]"
    }
}""" )
        
    
    ##
    addLine( f, """defined_text = { 
    name = MONSTER_NAME_ATTACKER
    text = {
        trigger = {
            check_variable = { attacker:actor_template^a = 1 }
        }
        localization_key = "[?attacker.GetLeader]"
    }""" )
    
    
    for row in values:
        count = int( row[ 1 ] )
        start = int( row[ 0 ] ) * 100
        for i in range( count ):
            
            addLine( f, """\
    text = {
        trigger = {
            check_variable = { attacker:actor_name^a = %s }
        }
        localization_key = "%s"
    }""" % ( str( i + start ), row[ 2 + i ] ) )
    
    addLine( f, """\
    text = {
        localization_key = "a=[?actor_name^a]"
    }
}""" )
        
    
    ##
    addLine( f, """defined_text = { 
    name = MONSTER_NAME_DEFENDER
    text = {
        trigger = {
            check_variable = { defender:actor_template^a = 1 }
        }
        localization_key = "[?defender.GetLeader]"
    }""" )
    
    
    for row in values:
        count = int( row[ 1 ] )
        start = int( row[ 0 ] ) * 100
        for i in range( count ):
            
            addLine( f, """\
    text = {
        trigger = {
            check_variable = { defender:actor_name^a = %s }
        }
        localization_key = "%s"
    }""" % ( str( i + start ), row[ 2 + i ] ) )
    
    addLine( f, """\
    text = {
        localization_key = "a=[?actor_name^a]"
    }
}""" )
        
    
    
if __name__ == '__main__':
    main()

