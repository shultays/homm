# adds equipment stats to actor stats

set_base_equipment_stat_1_Base_Damage = {
    set_variable = { actor_stats_damage_min^a = t_a }
    set_variable = { actor_stats_damage_max^a = t_b }
    set_variable = { actor_stats_attack_speed^a = t_c }
}

add_equipment_stat_1_Base_Damage = {
    # invalid
}

add_equipment_stat_2_Armor = {
    add_to_variable = { actor_stats_armor^a = t_a }
}

add_equipment_stat_3_Fire_Damage = {
    add_to_variable = { actor_stats_fire_damage_min^a = t_a }
    add_to_variable = { actor_stats_fire_damage_max^a = t_b }
}

add_equipment_stat_4_Lighting_Damage = {
    add_to_variable = { actor_stats_lighting_damage_min^a = t_a }
    add_to_variable = { actor_stats_lighting_damage_max^a = t_b }
}

add_equipment_stat_5_Cold_Damage = {
    add_to_variable = { actor_stats_cold_damage_min^a = t_a }
    add_to_variable = { actor_stats_cold_damage_max^a = t_b }
}

add_equipment_stat_6_Poison_Damage = {
    # handled separately
}

add_equipment_stat_7_Fire_Resist = {
    add_to_variable = { actor_stats_fire_resistance^a = t_a }
}

add_equipment_stat_8_Lighting_Resist = {
    add_to_variable = { actor_stats_lighting_resistance^a = t_a }
}

add_equipment_stat_9_Cold_Resist = {
    add_to_variable = { actor_stats_cold_resistance^a = t_a }
}

add_equipment_stat_10_Poison_Resist = {
    add_to_variable = { actor_stats_poison_resistance^a = t_a }
}

add_equipment_stat_11_Evasion = {
    set_temp_variable = { t = 1 }
    subtract_from_temp_variable = { t = t_a }
    multiply_temp_variable = { to_not_evade_chance = t }
}

add_equipment_stat_12_Health = {
    add_to_variable = { actor_stats_max_life^a = t_a }
}

add_equipment_stat_13_Life_Steal = {
    # handled separately
}

add_equipment_stat_14_Critical = {
    # handled separately
}

add_equipment_stat_15_Str = {
    add_to_variable = { total_strength = t_a }
}

add_equipment_stat_16_Dex = {
    add_to_variable = { total_dexterity = t_a }
}

add_equipment_stat_17_Vit = {
    add_to_variable = { total_vitality = t_a }
}

add_equipment_stat_18_Mag = {
    add_to_variable = { total_energy = t_a }
}

add_equipment_stat_19_Attack_Speed = {
    add_to_variable = { extra_attack_speed = t_a }
}

add_equipment_stat_20_Move_Speed = {
    add_to_temp_variable = { added_move_speed = t_a }
}

add_equipment_stat_21_Mana_Steal = {
    # handled separately
}

add_equipment_stat_22_Cast_Spell_on_attack = {
    # handled separately
}

add_equipment_stat_23_Cast_Spell_on_hit = {
    # handled separately
}

add_equipment_stat_24_Damage_Return = {
    # handled separately
}

add_equipment_stat_25_Block = {
    set_temp_variable = { t = 100 }
    subtract_from_temp_variable = { t = t_a }
    divide_temp_variable = { t = 100 }
    multiply_temp_variable = { to_not_block_chance = t }
}

add_equipment_stat_26_Pure_Damage = {
    add_to_variable = { actor_stats_pure_damage_min^a = t_a }
    add_to_variable = { actor_stats_pure_damage_max^a = t_b }
}

add_equipment_stat_27_Extra_Damage = {
    add_to_temp_variable = { extra_damage_mult = t_a }
}

add_equipment_stat_28_Life_Generation = {
    add_to_temp_variable = { extra_life_regen = t_a }
}

add_equipment_stat_29_Magic_Resist = {
    add_to_variable = { actor_stats_magic_resistance^a = t_a }
}

add_equipment_stat_30_Mana = {
    add_to_variable = { actor_stats_max_mana^a = t_a }
}

set_base_equipment_stat_31_Base_Move_Speed = {
    set_temp_variable = { base_move_speed = t_a }
    
    add_to_variable = { actor_stats_armor^a = t_b }
}

add_equipment_stat_31_Base_Move_Speed = {
    #invalid
}

add_equipment_stat_32_Base_Chance_To_Block = {
    set_temp_variable = { t = 100 }
    subtract_from_temp_variable = { t = t_a }
    divide_temp_variable = { t = 100 }
    multiply_temp_variable = { to_not_block_chance = t }
    
    add_to_variable = { actor_stats_armor^a = t_b }
}

add_equipment_stat_33_Mana_Regen = {
    add_to_temp_variable = { extra_mana_regen = t_a }
}
