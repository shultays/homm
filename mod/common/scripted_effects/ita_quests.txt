# italy quests

init_ita_quests = {
    ITA = {
        set_variable = { selected_quest = 0 }
        clear_array = quests
        add_to_array = { quests = 1 }
        add_to_array = { quests = 2 }
        add_to_array = { quests = 3 }
        add_to_array = { quests = 4 }
        add_to_array = { quests = 5 }
        add_to_array = { quests = 6 }
        
        build_quests = yes
        
        set_temp_variable = { t = 0 }
        enable_quest = yes
        select_quest = yes
    }
}


q1_e = {
    set_temp_variable = { t = 1 }
    enable_quest = yes
    select_quest = yes
}

q2_e = {

}

q3_e = {

}

q4_e = {

}

q5_e = {

}

q6_e = {

}
