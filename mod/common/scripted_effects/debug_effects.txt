# debug code and console commands

teleport_hero_test = {
    set_temp_variable = { h = from }
    set_temp_variable = { s = this }
    teleport_hero = yes
}

get_args_item_level = {
    set_temp_variable = { il = actor_level^0 }
    if = {
        limit = { check_variable = { args^num > 0 } }
        set_temp_variable = { il = args^0 }
    }
}

d_create_all = {
    d_create_inventory = yes
    d_create_equipment = yes
}

d_create_state_monsters = {
    get_args_item_level = yes
    set_temp_variable = { spawn_level = il }
    spawn_leveled_monsters = yes
}

d_create_monsters = {
    get_args_item_level = yes
    set_temp_variable = { spawn_level = il }
    set_temp_variable = { num_to_create = 5 }
    if = {
        limit = { check_variable = { args^num > 1 } }
        set_temp_variable = { num_to_create = args^1 }
    }

    for_loop_effect = {
        end = num_to_create

        add_random_monster = yes
    }
}

d_create_inventory = {
    ai_clear_inventory = yes
    get_args_item_level = yes

    set_temp_variable = { num_empty = 0 }
    for_loop_effect = {
        end = 32
        if = {
            limit = { check_variable = { inventory^v = 0 } }
            add_to_temp_variable = { num_empty = 1 }
        }
    }

    set_temp_variable = { num_to_create = num_empty }
    if = {
        limit = { check_variable = { args^num > 1 } }
        set_temp_variable = { num_to_create = args^1 }
        clamp_temp_variable = {
            var = num_to_create
            max = num_empty
        }
    }
    
    for_loop_effect = {
        end = num_to_create
        set_temp_variable = { item_type = 10 }
        create_leveled_item_with_type = yes
        #create_leveled_item = yes
        
        add_last_item_to_inventory = yes
    }
}

d_create_equipment = {
    get_args_item_level = yes

    if = {
        limit = { check_variable = { inventory^32 = 0 } }
        set_temp_variable = { item_type = 1 }
        create_leveled_item_with_type = yes
        get_last_added_item = yes
        set_variable = { inventory^32 = t }
    }
    
    if = {
        limit = { check_variable = { inventory^33 = 0 } }
        set_temp_variable = { item_type = 2 }
        create_leveled_item_with_type = yes
        get_last_added_item = yes
        set_variable = { inventory^33 = t }
    }
    
    if = {
        limit = { check_variable = { inventory^34 = 0 } }
        set_temp_variable = { item_type = 3 }
        create_leveled_item_with_type = yes
        get_last_added_item = yes
        set_variable = { inventory^34 = t }
    }
    
    if = {
        limit = { check_variable = { inventory^35 = 0 } }
        set_temp_variable = { item_type = 4 }
        create_leveled_item_with_type = yes
        get_last_added_item = yes
        set_variable = { inventory^35 = t }
    }
    
    if = {
        limit = { check_variable = { inventory^36 = 0 } }
        set_temp_variable = { item_type = 5 }
        create_leveled_item_with_type = yes
        get_last_added_item = yes
        set_variable = { inventory^36 = t }
    }
    
    if = {
        limit = { check_variable = { inventory^41 = 0 } }
        set_temp_variable = { item_type = 6 }
        create_leveled_item_with_type = yes
        get_last_added_item = yes
        set_variable = { inventory^41 = t }
    }
    
    if = {
        limit = { check_variable = { inventory^42 = 0 } }
        set_temp_variable = { item_type = 6 }
        create_leveled_item_with_type = yes
        get_last_added_item = yes
        set_variable = { inventory^42 = t }
    }
    
    if = {
        limit = { check_variable = { inventory^38 = 0 } }
        set_temp_variable = { item_type = 7 }
        create_leveled_item_with_type = yes
        get_last_added_item = yes
        set_variable = { inventory^38 = t }
    }
    
    if = {
        limit = { check_variable = { inventory^39 = 0 } }
        set_temp_variable = { item_type = 8 }
        create_leveled_item_with_type = yes
        get_last_added_item = yes
        set_variable = { inventory^39 = t }
    }
    
    if = {
        limit = { check_variable = { inventory^40 = 0 } }
        set_temp_variable = { item_type = 9 }
        create_leveled_item_with_type = yes
        get_last_added_item = yes
        set_variable = { inventory^40 = t }
    }
}

d_test_combat = {
    set_temp_variable = { monster_str = actor_level^0 }
    var:root_capital = {
        spawn_leveled_monsters = yes
    }
    
    set_temp_variable = { side_a = root }
    set_temp_variable = { side_b = var:root_capital }
    create_combat = yes
}

d_build_leveling_item = {
    set_temp_variable = { n = 2 }
    get_args_item_level = yes
    set_temp_variable = { item_build_level = il }
    build_leveling_item = yes
}

d_update_longinus = {
    set_temp_variable = { q_n = 2 }
    update_longinus_souls = yes
}

d_test_levelup = {
    set_temp_variable = { ai_test = 1 }

    set_temp_variable = { __t = 10 }

    set_temp_variable = { num_won = 0 }
    set_temp_variable = { num_lost = 0 }

    set_temp_variable = { __nerf = 15 }

    set_temp_variable = { __t_w = total_combat_won }
    multiply_temp_variable = { __t_w = -1 }
    set_temp_variable = { __t_l = total_combat_lost }
    multiply_temp_variable = { __t_l = -1 }

    while_loop_effect = {
        limit = { check_variable = { __t > 0 } }
        subtract_from_temp_variable = { __t = 1 }

        set_temp_variable = { __t_a = total_combat_won }

        set_temp_variable = { __t2 = 10 }
        while_loop_effect = {
            limit = { check_variable = { __t2 > 0 } }
            subtract_from_temp_variable = { __t2 = 1 }

            set_temp_variable = { monster_str = actor_level^0 }
            subtract_from_temp_variable = { monster_str = __nerf }
            clamp_temp_variable = {
                var = monster_str 
                min = 1
            }

            var:root_capital = {
                if = {
                    limit = { check_variable = { actor_level^num > 0 } }
                    clear_state_actors = yes
                }
                spawn_leveled_monsters = yes
            }

            set_temp_variable = { __t_a = total_combat_won }

            set_temp_variable = { side_a = root }
            set_temp_variable = { side_b = var:root_capital }
            create_combat = yes

            while_loop_effect = {
                limit = { check_variable = { combat_started = 1 } }
                tick_combat = yes
            }

            if = {
                limit = { check_variable = { __t_a = total_combat_won } }
                add_to_temp_variable = { __nerf = 0.2 }
                multiply_temp_variable = { __nerf = 1.2 }

                else = {
                    subtract_from_temp_variable = { __nerf = 0.15 }
                    divide_temp_variable = { __nerf = 1.1 }
                    clamp_temp_variable = {
                        var = __nerf 
                        min = 0
                    }
                }
            }
        }
        
        log = "[?__nerf]"
    }

    add_to_temp_variable = { __t_w = total_combat_won }
    add_to_temp_variable = { __t_l = total_combat_lost }

    log = "won: [?__t_w]"
    log = "lost: [?__t_l]"

}

d_test_path_find = {
    set_temp_variable = { s0 = from.capital }
    set_temp_variable = { s1 = this }
    log = "[?s0.GetName] [?s1.GetName]"
    from = {
        path_find = yes
    }
    
    if = {
        limit = { check_variable = { path^num = 0 } }
        log = "Not Found"
    }
    else = {
        for_each_scope_loop = {
            array = path
            for_each_scope_loop = {
                array = path
                log = "[?this.GetName]"
            }
        }
    }
}