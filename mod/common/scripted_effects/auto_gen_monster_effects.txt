init_monster_spawns = {
clear_array = global.monster_spawn_t1
clear_array = global.monster_spawn_t2
# kobold 
add_to_array = { global.monster_spawn_t1 = 2 }
add_to_array = { global.monster_spawn_t2 = 3 }
# orc 
add_to_array = { global.monster_spawn_t1 = 4 }
add_to_array = { global.monster_spawn_t2 = 5 }
# animal 
add_to_array = { global.monster_spawn_t1 = 6 }
add_to_array = { global.monster_spawn_t2 = 7 }
}
