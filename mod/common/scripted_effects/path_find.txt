
# finds a path between two states and stores result in temp array "path"
# s0 s1
# path is reversed (goes from s1 to s0)
# path is empty if no path found
path_find = {
    clear_temp_array = que
    
    add_to_variable = { global.path_id = 0.001 }
    set_temp_variable = { t = global.path_id }
    
    add_to_temp_array = { que = s0 }
    var:s0 = {
        set_variable = { visited = t }
        set_variable = { pr = -1 }
    }

    set_temp_variable = { found = 0 }
    while_loop_effect = {
        limit = { 
            check_variable = { que^num > 0 } 
            check_variable = { found = 0 } 
        }
        set_temp_variable = { s = que^0 }
        remove_from_temp_array = {
            array = que
            index = 0
        }
        
        var:s = {
            every_neighbor_state = {
                if = {
                    limit = { 
                        not = { check_variable = { visited = t } } 
                        is_controlled_by = prev
                    }

                    add_to_temp_array = { que = this }
                    set_variable = { visited = t }
                    set_variable = { pr = s }
                    
                    if = {
                        limit = { state = var:s1 }
                        set_temp_variable = { found = 1 }
                    }
                }
            }
        }
    }

    clear_temp_array = path
    if = {
        limit = { check_variable = { found = 1 } }

        set_temp_variable = { s = s1 }
        
        while_loop_effect = {
            limit = { not = { check_variable = { s = -1 } } }
            
            add_to_temp_array = { path = s }
            set_temp_variable = { s = s:pr }
        }
    }
}