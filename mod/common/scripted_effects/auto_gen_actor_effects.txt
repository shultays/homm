create_new_actor = {
add_to_array = { actor_battle = 0 }
add_to_array = { actor_name = 0 }
add_to_array = { actor_name_pre = 0 }
add_to_array = { actor_name_post = 0 }
add_to_array = { actor_icon = 0 }
add_to_array = { actor_level = 0 }
add_to_array = { actor_template = 0 }
add_to_array = { actor_life = 0 }
add_to_array = { actor_mana = 0 }
add_to_array = { rec_damage_mult = 0 }
add_to_array = { attack_cost = 0 }
add_to_array = { actor_template_2 = 0 }
add_to_array = { actor_template_2_weight = 0 }
add_to_array = { actor_type = 0 }
add_to_array = { actor_entity = 0 }
add_to_array = { actor_stats_actor_level = 0 }
add_to_array = { actor_stats_max_life = 0 }
add_to_array = { actor_stats_max_mana = 0 }
add_to_array = { actor_stats_armor = 0 }
add_to_array = { actor_stats_evasion = 0 }
add_to_array = { actor_stats_block = 0 }
add_to_array = { actor_stats_damage_min = 0 }
add_to_array = { actor_stats_damage_max = 0 }
add_to_array = { actor_stats_attack_speed = 0 }
add_to_array = { actor_stats_move_speed = 0 }
add_to_array = { actor_stats_fire_resistance = 0 }
add_to_array = { actor_stats_cold_resistance = 0 }
add_to_array = { actor_stats_lighting_resistance = 0 }
add_to_array = { actor_stats_magic_resistance = 0 }
add_to_array = { actor_stats_poison_resistance = 0 }
add_to_array = { actor_stats_fire_damage_min = 0 }
add_to_array = { actor_stats_fire_damage_max = 0 }
add_to_array = { actor_stats_cold_damage_min = 0 }
add_to_array = { actor_stats_cold_damage_max = 0 }
add_to_array = { actor_stats_lighting_damage_min = 0 }
add_to_array = { actor_stats_lighting_damage_max = 0 }
add_to_array = { actor_stats_life_steal_mult = 0 }
add_to_array = { actor_stats_life_steal_chance = 0 }
add_to_array = { actor_stats_mana_steal_mult = 0 }
add_to_array = { actor_stats_mana_steal_chance = 0 }
add_to_array = { actor_stats_pure_damage_min = 0 }
add_to_array = { actor_stats_pure_damage_max = 0 }
add_to_array = { actor_stats_critical_mult = 0 }
add_to_array = { actor_stats_critical_chance = 0 }
add_to_array = { actor_stats_mana_regen = 0 }
add_to_array = { actor_stats_life_regen = 0 }
}
remove_actor = {
remove_from_array = { array = actor_battle index = a }
remove_from_array = { array = actor_name index = a }
remove_from_array = { array = actor_name_pre index = a }
remove_from_array = { array = actor_name_post index = a }
remove_from_array = { array = actor_icon index = a }
remove_from_array = { array = actor_level index = a }
remove_from_array = { array = actor_template index = a }
remove_from_array = { array = actor_life index = a }
remove_from_array = { array = actor_mana index = a }
remove_from_array = { array = rec_damage_mult index = a }
remove_from_array = { array = attack_cost index = a }
remove_from_array = { array = actor_template_2 index = a }
remove_from_array = { array = actor_template_2_weight index = a }
remove_from_array = { array = actor_type index = a }
remove_from_array = { array = actor_entity index = a }
remove_from_array = { array = actor_stats_actor_level index = a }
remove_from_array = { array = actor_stats_max_life index = a }
remove_from_array = { array = actor_stats_max_mana index = a }
remove_from_array = { array = actor_stats_armor index = a }
remove_from_array = { array = actor_stats_evasion index = a }
remove_from_array = { array = actor_stats_block index = a }
remove_from_array = { array = actor_stats_damage_min index = a }
remove_from_array = { array = actor_stats_damage_max index = a }
remove_from_array = { array = actor_stats_attack_speed index = a }
remove_from_array = { array = actor_stats_move_speed index = a }
remove_from_array = { array = actor_stats_fire_resistance index = a }
remove_from_array = { array = actor_stats_cold_resistance index = a }
remove_from_array = { array = actor_stats_lighting_resistance index = a }
remove_from_array = { array = actor_stats_magic_resistance index = a }
remove_from_array = { array = actor_stats_poison_resistance index = a }
remove_from_array = { array = actor_stats_fire_damage_min index = a }
remove_from_array = { array = actor_stats_fire_damage_max index = a }
remove_from_array = { array = actor_stats_cold_damage_min index = a }
remove_from_array = { array = actor_stats_cold_damage_max index = a }
remove_from_array = { array = actor_stats_lighting_damage_min index = a }
remove_from_array = { array = actor_stats_lighting_damage_max index = a }
remove_from_array = { array = actor_stats_life_steal_mult index = a }
remove_from_array = { array = actor_stats_life_steal_chance index = a }
remove_from_array = { array = actor_stats_mana_steal_mult index = a }
remove_from_array = { array = actor_stats_mana_steal_chance index = a }
remove_from_array = { array = actor_stats_pure_damage_min index = a }
remove_from_array = { array = actor_stats_pure_damage_max index = a }
remove_from_array = { array = actor_stats_critical_mult index = a }
remove_from_array = { array = actor_stats_critical_chance index = a }
remove_from_array = { array = actor_stats_mana_regen index = a }
remove_from_array = { array = actor_stats_life_regen index = a }
}
swap_actors = {
set_variable = { swap_t = actor_battle^a }
set_variable = { actor_battle^a = actor_battle^b }
set_variable = { actor_battle^b = swap_t }
set_variable = { swap_t = actor_name^a }
set_variable = { actor_name^a = actor_name^b }
set_variable = { actor_name^b = swap_t }
set_variable = { swap_t = actor_name_pre^a }
set_variable = { actor_name_pre^a = actor_name_pre^b }
set_variable = { actor_name_pre^b = swap_t }
set_variable = { swap_t = actor_name_post^a }
set_variable = { actor_name_post^a = actor_name_post^b }
set_variable = { actor_name_post^b = swap_t }
set_variable = { swap_t = actor_icon^a }
set_variable = { actor_icon^a = actor_icon^b }
set_variable = { actor_icon^b = swap_t }
set_variable = { swap_t = actor_level^a }
set_variable = { actor_level^a = actor_level^b }
set_variable = { actor_level^b = swap_t }
set_variable = { swap_t = actor_template^a }
set_variable = { actor_template^a = actor_template^b }
set_variable = { actor_template^b = swap_t }
set_variable = { swap_t = actor_life^a }
set_variable = { actor_life^a = actor_life^b }
set_variable = { actor_life^b = swap_t }
set_variable = { swap_t = actor_mana^a }
set_variable = { actor_mana^a = actor_mana^b }
set_variable = { actor_mana^b = swap_t }
set_variable = { swap_t = rec_damage_mult^a }
set_variable = { rec_damage_mult^a = rec_damage_mult^b }
set_variable = { rec_damage_mult^b = swap_t }
set_variable = { swap_t = attack_cost^a }
set_variable = { attack_cost^a = attack_cost^b }
set_variable = { attack_cost^b = swap_t }
set_variable = { swap_t = actor_template_2^a }
set_variable = { actor_template_2^a = actor_template_2^b }
set_variable = { actor_template_2^b = swap_t }
set_variable = { swap_t = actor_template_2_weight^a }
set_variable = { actor_template_2_weight^a = actor_template_2_weight^b }
set_variable = { actor_template_2_weight^b = swap_t }
set_variable = { swap_t = actor_type^a }
set_variable = { actor_type^a = actor_type^b }
set_variable = { actor_type^b = swap_t }
set_variable = { swap_t = actor_entity^a }
set_variable = { actor_entity^a = actor_entity^b }
set_variable = { actor_entity^b = swap_t }
set_variable = { swap_t = actor_stats_actor_level^a }
set_variable = { actor_stats_actor_level^a = actor_stats_actor_level^b }
set_variable = { actor_stats_actor_level^b = swap_t }
set_variable = { swap_t = actor_stats_max_life^a }
set_variable = { actor_stats_max_life^a = actor_stats_max_life^b }
set_variable = { actor_stats_max_life^b = swap_t }
set_variable = { swap_t = actor_stats_max_mana^a }
set_variable = { actor_stats_max_mana^a = actor_stats_max_mana^b }
set_variable = { actor_stats_max_mana^b = swap_t }
set_variable = { swap_t = actor_stats_armor^a }
set_variable = { actor_stats_armor^a = actor_stats_armor^b }
set_variable = { actor_stats_armor^b = swap_t }
set_variable = { swap_t = actor_stats_evasion^a }
set_variable = { actor_stats_evasion^a = actor_stats_evasion^b }
set_variable = { actor_stats_evasion^b = swap_t }
set_variable = { swap_t = actor_stats_block^a }
set_variable = { actor_stats_block^a = actor_stats_block^b }
set_variable = { actor_stats_block^b = swap_t }
set_variable = { swap_t = actor_stats_damage_min^a }
set_variable = { actor_stats_damage_min^a = actor_stats_damage_min^b }
set_variable = { actor_stats_damage_min^b = swap_t }
set_variable = { swap_t = actor_stats_damage_max^a }
set_variable = { actor_stats_damage_max^a = actor_stats_damage_max^b }
set_variable = { actor_stats_damage_max^b = swap_t }
set_variable = { swap_t = actor_stats_attack_speed^a }
set_variable = { actor_stats_attack_speed^a = actor_stats_attack_speed^b }
set_variable = { actor_stats_attack_speed^b = swap_t }
set_variable = { swap_t = actor_stats_move_speed^a }
set_variable = { actor_stats_move_speed^a = actor_stats_move_speed^b }
set_variable = { actor_stats_move_speed^b = swap_t }
set_variable = { swap_t = actor_stats_fire_resistance^a }
set_variable = { actor_stats_fire_resistance^a = actor_stats_fire_resistance^b }
set_variable = { actor_stats_fire_resistance^b = swap_t }
set_variable = { swap_t = actor_stats_cold_resistance^a }
set_variable = { actor_stats_cold_resistance^a = actor_stats_cold_resistance^b }
set_variable = { actor_stats_cold_resistance^b = swap_t }
set_variable = { swap_t = actor_stats_lighting_resistance^a }
set_variable = { actor_stats_lighting_resistance^a = actor_stats_lighting_resistance^b }
set_variable = { actor_stats_lighting_resistance^b = swap_t }
set_variable = { swap_t = actor_stats_magic_resistance^a }
set_variable = { actor_stats_magic_resistance^a = actor_stats_magic_resistance^b }
set_variable = { actor_stats_magic_resistance^b = swap_t }
set_variable = { swap_t = actor_stats_poison_resistance^a }
set_variable = { actor_stats_poison_resistance^a = actor_stats_poison_resistance^b }
set_variable = { actor_stats_poison_resistance^b = swap_t }
set_variable = { swap_t = actor_stats_fire_damage_min^a }
set_variable = { actor_stats_fire_damage_min^a = actor_stats_fire_damage_min^b }
set_variable = { actor_stats_fire_damage_min^b = swap_t }
set_variable = { swap_t = actor_stats_fire_damage_max^a }
set_variable = { actor_stats_fire_damage_max^a = actor_stats_fire_damage_max^b }
set_variable = { actor_stats_fire_damage_max^b = swap_t }
set_variable = { swap_t = actor_stats_cold_damage_min^a }
set_variable = { actor_stats_cold_damage_min^a = actor_stats_cold_damage_min^b }
set_variable = { actor_stats_cold_damage_min^b = swap_t }
set_variable = { swap_t = actor_stats_cold_damage_max^a }
set_variable = { actor_stats_cold_damage_max^a = actor_stats_cold_damage_max^b }
set_variable = { actor_stats_cold_damage_max^b = swap_t }
set_variable = { swap_t = actor_stats_lighting_damage_min^a }
set_variable = { actor_stats_lighting_damage_min^a = actor_stats_lighting_damage_min^b }
set_variable = { actor_stats_lighting_damage_min^b = swap_t }
set_variable = { swap_t = actor_stats_lighting_damage_max^a }
set_variable = { actor_stats_lighting_damage_max^a = actor_stats_lighting_damage_max^b }
set_variable = { actor_stats_lighting_damage_max^b = swap_t }
set_variable = { swap_t = actor_stats_life_steal_mult^a }
set_variable = { actor_stats_life_steal_mult^a = actor_stats_life_steal_mult^b }
set_variable = { actor_stats_life_steal_mult^b = swap_t }
set_variable = { swap_t = actor_stats_life_steal_chance^a }
set_variable = { actor_stats_life_steal_chance^a = actor_stats_life_steal_chance^b }
set_variable = { actor_stats_life_steal_chance^b = swap_t }
set_variable = { swap_t = actor_stats_mana_steal_mult^a }
set_variable = { actor_stats_mana_steal_mult^a = actor_stats_mana_steal_mult^b }
set_variable = { actor_stats_mana_steal_mult^b = swap_t }
set_variable = { swap_t = actor_stats_mana_steal_chance^a }
set_variable = { actor_stats_mana_steal_chance^a = actor_stats_mana_steal_chance^b }
set_variable = { actor_stats_mana_steal_chance^b = swap_t }
set_variable = { swap_t = actor_stats_pure_damage_min^a }
set_variable = { actor_stats_pure_damage_min^a = actor_stats_pure_damage_min^b }
set_variable = { actor_stats_pure_damage_min^b = swap_t }
set_variable = { swap_t = actor_stats_pure_damage_max^a }
set_variable = { actor_stats_pure_damage_max^a = actor_stats_pure_damage_max^b }
set_variable = { actor_stats_pure_damage_max^b = swap_t }
set_variable = { swap_t = actor_stats_critical_mult^a }
set_variable = { actor_stats_critical_mult^a = actor_stats_critical_mult^b }
set_variable = { actor_stats_critical_mult^b = swap_t }
set_variable = { swap_t = actor_stats_critical_chance^a }
set_variable = { actor_stats_critical_chance^a = actor_stats_critical_chance^b }
set_variable = { actor_stats_critical_chance^b = swap_t }
set_variable = { swap_t = actor_stats_mana_regen^a }
set_variable = { actor_stats_mana_regen^a = actor_stats_mana_regen^b }
set_variable = { actor_stats_mana_regen^b = swap_t }
set_variable = { swap_t = actor_stats_life_regen^a }
set_variable = { actor_stats_life_regen^a = actor_stats_life_regen^b }
set_variable = { actor_stats_life_regen^b = swap_t }
}
copy_actor_from_other = {
add_to_array = { actor_battle = other:actor_battle^a }
add_to_array = { actor_name = other:actor_name^a }
add_to_array = { actor_name_pre = other:actor_name_pre^a }
add_to_array = { actor_name_post = other:actor_name_post^a }
add_to_array = { actor_icon = other:actor_icon^a }
add_to_array = { actor_level = other:actor_level^a }
add_to_array = { actor_template = other:actor_template^a }
add_to_array = { actor_life = other:actor_life^a }
add_to_array = { actor_mana = other:actor_mana^a }
add_to_array = { rec_damage_mult = other:rec_damage_mult^a }
add_to_array = { attack_cost = other:attack_cost^a }
add_to_array = { actor_template_2 = other:actor_template_2^a }
add_to_array = { actor_template_2_weight = other:actor_template_2_weight^a }
add_to_array = { actor_type = other:actor_type^a }
add_to_array = { actor_entity = other:actor_entity^a }
add_to_array = { actor_stats_actor_level = other:actor_stats_actor_level^a }
add_to_array = { actor_stats_max_life = other:actor_stats_max_life^a }
add_to_array = { actor_stats_max_mana = other:actor_stats_max_mana^a }
add_to_array = { actor_stats_armor = other:actor_stats_armor^a }
add_to_array = { actor_stats_evasion = other:actor_stats_evasion^a }
add_to_array = { actor_stats_block = other:actor_stats_block^a }
add_to_array = { actor_stats_damage_min = other:actor_stats_damage_min^a }
add_to_array = { actor_stats_damage_max = other:actor_stats_damage_max^a }
add_to_array = { actor_stats_attack_speed = other:actor_stats_attack_speed^a }
add_to_array = { actor_stats_move_speed = other:actor_stats_move_speed^a }
add_to_array = { actor_stats_fire_resistance = other:actor_stats_fire_resistance^a }
add_to_array = { actor_stats_cold_resistance = other:actor_stats_cold_resistance^a }
add_to_array = { actor_stats_lighting_resistance = other:actor_stats_lighting_resistance^a }
add_to_array = { actor_stats_magic_resistance = other:actor_stats_magic_resistance^a }
add_to_array = { actor_stats_poison_resistance = other:actor_stats_poison_resistance^a }
add_to_array = { actor_stats_fire_damage_min = other:actor_stats_fire_damage_min^a }
add_to_array = { actor_stats_fire_damage_max = other:actor_stats_fire_damage_max^a }
add_to_array = { actor_stats_cold_damage_min = other:actor_stats_cold_damage_min^a }
add_to_array = { actor_stats_cold_damage_max = other:actor_stats_cold_damage_max^a }
add_to_array = { actor_stats_lighting_damage_min = other:actor_stats_lighting_damage_min^a }
add_to_array = { actor_stats_lighting_damage_max = other:actor_stats_lighting_damage_max^a }
add_to_array = { actor_stats_life_steal_mult = other:actor_stats_life_steal_mult^a }
add_to_array = { actor_stats_life_steal_chance = other:actor_stats_life_steal_chance^a }
add_to_array = { actor_stats_mana_steal_mult = other:actor_stats_mana_steal_mult^a }
add_to_array = { actor_stats_mana_steal_chance = other:actor_stats_mana_steal_chance^a }
add_to_array = { actor_stats_pure_damage_min = other:actor_stats_pure_damage_min^a }
add_to_array = { actor_stats_pure_damage_max = other:actor_stats_pure_damage_max^a }
add_to_array = { actor_stats_critical_mult = other:actor_stats_critical_mult^a }
add_to_array = { actor_stats_critical_chance = other:actor_stats_critical_chance^a }
add_to_array = { actor_stats_mana_regen = other:actor_stats_mana_regen^a }
add_to_array = { actor_stats_life_regen = other:actor_stats_life_regen^a }
}

set_actor_to_template = {
set_temp_variable = { e = actor_template^a }
set_variable = { actor_stats_actor_level^a = global.actor_template_base_actor_level^e }
set_variable = { actor_stats_max_life^a = global.actor_template_base_max_life^e }
set_variable = { actor_stats_max_mana^a = global.actor_template_base_max_mana^e }
set_variable = { actor_stats_armor^a = global.actor_template_base_armor^e }
set_variable = { actor_stats_evasion^a = global.actor_template_base_evasion^e }
set_variable = { actor_stats_block^a = global.actor_template_base_block^e }
set_variable = { actor_stats_damage_min^a = global.actor_template_base_damage_min^e }
set_variable = { actor_stats_damage_max^a = global.actor_template_base_damage_max^e }
set_variable = { actor_stats_attack_speed^a = global.actor_template_base_attack_speed^e }
set_variable = { actor_stats_move_speed^a = global.actor_template_base_move_speed^e }
set_variable = { actor_stats_fire_resistance^a = global.actor_template_base_fire_resistance^e }
set_variable = { actor_stats_cold_resistance^a = global.actor_template_base_cold_resistance^e }
set_variable = { actor_stats_lighting_resistance^a = global.actor_template_base_lighting_resistance^e }
set_variable = { actor_stats_magic_resistance^a = global.actor_template_base_magic_resistance^e }
set_variable = { actor_stats_poison_resistance^a = global.actor_template_base_poison_resistance^e }
set_variable = { actor_stats_fire_damage_min^a = global.actor_template_base_fire_damage_min^e }
set_variable = { actor_stats_fire_damage_max^a = global.actor_template_base_fire_damage_max^e }
set_variable = { actor_stats_cold_damage_min^a = global.actor_template_base_cold_damage_min^e }
set_variable = { actor_stats_cold_damage_max^a = global.actor_template_base_cold_damage_max^e }
set_variable = { actor_stats_lighting_damage_min^a = global.actor_template_base_lighting_damage_min^e }
set_variable = { actor_stats_lighting_damage_max^a = global.actor_template_base_lighting_damage_max^e }
set_variable = { actor_stats_life_steal_mult^a = global.actor_template_base_life_steal_mult^e }
set_variable = { actor_stats_life_steal_chance^a = global.actor_template_base_life_steal_chance^e }
set_variable = { actor_stats_mana_steal_mult^a = global.actor_template_base_mana_steal_mult^e }
set_variable = { actor_stats_mana_steal_chance^a = global.actor_template_base_mana_steal_chance^e }
set_variable = { actor_stats_pure_damage_min^a = global.actor_template_base_pure_damage_min^e }
set_variable = { actor_stats_pure_damage_max^a = global.actor_template_base_pure_damage_max^e }
set_variable = { actor_stats_critical_mult^a = global.actor_template_base_critical_mult^e }
set_variable = { actor_stats_critical_chance^a = global.actor_template_base_critical_chance^e }
set_variable = { actor_stats_mana_regen^a = global.actor_template_base_mana_regen^e }
set_variable = { actor_stats_life_regen^a = global.actor_template_base_life_regen^e }
}
clear_actors = {
clear_array = actor_battle
clear_array = actor_name
clear_array = actor_name_pre
clear_array = actor_name_post
clear_array = actor_icon
clear_array = actor_level
clear_array = actor_template
clear_array = actor_life
clear_array = actor_mana
clear_array = rec_damage_mult
clear_array = attack_cost
clear_array = actor_template_2
clear_array = actor_template_2_weight
clear_array = actor_type
clear_array = actor_entity
clear_array = actor_stats_actor_level
clear_array = actor_stats_max_life
clear_array = actor_stats_max_mana
clear_array = actor_stats_armor
clear_array = actor_stats_evasion
clear_array = actor_stats_block
clear_array = actor_stats_damage_min
clear_array = actor_stats_damage_max
clear_array = actor_stats_attack_speed
clear_array = actor_stats_move_speed
clear_array = actor_stats_fire_resistance
clear_array = actor_stats_cold_resistance
clear_array = actor_stats_lighting_resistance
clear_array = actor_stats_magic_resistance
clear_array = actor_stats_poison_resistance
clear_array = actor_stats_fire_damage_min
clear_array = actor_stats_fire_damage_max
clear_array = actor_stats_cold_damage_min
clear_array = actor_stats_cold_damage_max
clear_array = actor_stats_lighting_damage_min
clear_array = actor_stats_lighting_damage_max
clear_array = actor_stats_life_steal_mult
clear_array = actor_stats_life_steal_chance
clear_array = actor_stats_mana_steal_mult
clear_array = actor_stats_mana_steal_chance
clear_array = actor_stats_pure_damage_min
clear_array = actor_stats_pure_damage_max
clear_array = actor_stats_critical_mult
clear_array = actor_stats_critical_chance
clear_array = actor_stats_mana_regen
clear_array = actor_stats_life_regen
}
set_actor_to_template_weighted = {
set_temp_variable = { e_1 = actor_template^a }
set_temp_variable = { e_2 = actor_template_2^a }
set_temp_variable = { e_1_w = 1 }
subtract_from_temp_variable = { e_1_w = actor_template_2_weight^a }
set_temp_variable = { e_2_w = actor_template_2_weight^a }
if = {
    limit = { check_variable = { global.actor_template_base_actor_level^e_2 > 0 } }

    set_temp_variable = { t_1 = global.actor_template_base_actor_level^e_1 }
    multiply_temp_variable = { t_1 = e_1_w }
    set_temp_variable = { t_2 = global.actor_template_base_actor_level^e_2 }
    multiply_temp_variable = { t_2 = e_2_w }
    set_variable = { actor_stats_actor_level^a = t_1 }
    add_to_variable = { actor_stats_actor_level^a = t_2 }
}
else = {
    set_variable = { actor_stats_actor_level^a = global.actor_template_base_actor_level^e_1 }
}
if = {
    limit = { check_variable = { global.actor_template_base_max_life^e_2 > 0 } }

    set_temp_variable = { t_1 = global.actor_template_base_max_life^e_1 }
    multiply_temp_variable = { t_1 = e_1_w }
    set_temp_variable = { t_2 = global.actor_template_base_max_life^e_2 }
    multiply_temp_variable = { t_2 = e_2_w }
    set_variable = { actor_stats_max_life^a = t_1 }
    add_to_variable = { actor_stats_max_life^a = t_2 }
}
else = {
    set_variable = { actor_stats_max_life^a = global.actor_template_base_max_life^e_1 }
}
if = {
    limit = { check_variable = { global.actor_template_base_max_mana^e_2 > 0 } }

    set_temp_variable = { t_1 = global.actor_template_base_max_mana^e_1 }
    multiply_temp_variable = { t_1 = e_1_w }
    set_temp_variable = { t_2 = global.actor_template_base_max_mana^e_2 }
    multiply_temp_variable = { t_2 = e_2_w }
    set_variable = { actor_stats_max_mana^a = t_1 }
    add_to_variable = { actor_stats_max_mana^a = t_2 }
}
else = {
    set_variable = { actor_stats_max_mana^a = global.actor_template_base_max_mana^e_1 }
}
if = {
    limit = { check_variable = { global.actor_template_base_armor^e_2 > 0 } }

    set_temp_variable = { t_1 = global.actor_template_base_armor^e_1 }
    multiply_temp_variable = { t_1 = e_1_w }
    set_temp_variable = { t_2 = global.actor_template_base_armor^e_2 }
    multiply_temp_variable = { t_2 = e_2_w }
    set_variable = { actor_stats_armor^a = t_1 }
    add_to_variable = { actor_stats_armor^a = t_2 }
}
else = {
    set_variable = { actor_stats_armor^a = global.actor_template_base_armor^e_1 }
}
if = {
    limit = { check_variable = { global.actor_template_base_evasion^e_2 > 0 } }

    set_temp_variable = { t_1 = global.actor_template_base_evasion^e_1 }
    multiply_temp_variable = { t_1 = e_1_w }
    set_temp_variable = { t_2 = global.actor_template_base_evasion^e_2 }
    multiply_temp_variable = { t_2 = e_2_w }
    set_variable = { actor_stats_evasion^a = t_1 }
    add_to_variable = { actor_stats_evasion^a = t_2 }
}
else = {
    set_variable = { actor_stats_evasion^a = global.actor_template_base_evasion^e_1 }
}
if = {
    limit = { check_variable = { global.actor_template_base_block^e_2 > 0 } }

    set_temp_variable = { t_1 = global.actor_template_base_block^e_1 }
    multiply_temp_variable = { t_1 = e_1_w }
    set_temp_variable = { t_2 = global.actor_template_base_block^e_2 }
    multiply_temp_variable = { t_2 = e_2_w }
    set_variable = { actor_stats_block^a = t_1 }
    add_to_variable = { actor_stats_block^a = t_2 }
}
else = {
    set_variable = { actor_stats_block^a = global.actor_template_base_block^e_1 }
}
if = {
    limit = { check_variable = { global.actor_template_base_damage_min^e_2 > 0 } }

    set_temp_variable = { t_1 = global.actor_template_base_damage_min^e_1 }
    multiply_temp_variable = { t_1 = e_1_w }
    set_temp_variable = { t_2 = global.actor_template_base_damage_min^e_2 }
    multiply_temp_variable = { t_2 = e_2_w }
    set_variable = { actor_stats_damage_min^a = t_1 }
    add_to_variable = { actor_stats_damage_min^a = t_2 }
}
else = {
    set_variable = { actor_stats_damage_min^a = global.actor_template_base_damage_min^e_1 }
}
if = {
    limit = { check_variable = { global.actor_template_base_damage_max^e_2 > 0 } }

    set_temp_variable = { t_1 = global.actor_template_base_damage_max^e_1 }
    multiply_temp_variable = { t_1 = e_1_w }
    set_temp_variable = { t_2 = global.actor_template_base_damage_max^e_2 }
    multiply_temp_variable = { t_2 = e_2_w }
    set_variable = { actor_stats_damage_max^a = t_1 }
    add_to_variable = { actor_stats_damage_max^a = t_2 }
}
else = {
    set_variable = { actor_stats_damage_max^a = global.actor_template_base_damage_max^e_1 }
}
if = {
    limit = { check_variable = { global.actor_template_base_attack_speed^e_2 > 0 } }

    set_temp_variable = { t_1 = global.actor_template_base_attack_speed^e_1 }
    multiply_temp_variable = { t_1 = e_1_w }
    set_temp_variable = { t_2 = global.actor_template_base_attack_speed^e_2 }
    multiply_temp_variable = { t_2 = e_2_w }
    set_variable = { actor_stats_attack_speed^a = t_1 }
    add_to_variable = { actor_stats_attack_speed^a = t_2 }
}
else = {
    set_variable = { actor_stats_attack_speed^a = global.actor_template_base_attack_speed^e_1 }
}
if = {
    limit = { check_variable = { global.actor_template_base_move_speed^e_2 > 0 } }

    set_temp_variable = { t_1 = global.actor_template_base_move_speed^e_1 }
    multiply_temp_variable = { t_1 = e_1_w }
    set_temp_variable = { t_2 = global.actor_template_base_move_speed^e_2 }
    multiply_temp_variable = { t_2 = e_2_w }
    set_variable = { actor_stats_move_speed^a = t_1 }
    add_to_variable = { actor_stats_move_speed^a = t_2 }
}
else = {
    set_variable = { actor_stats_move_speed^a = global.actor_template_base_move_speed^e_1 }
}
if = {
    limit = { check_variable = { global.actor_template_base_fire_resistance^e_2 > 0 } }

    set_temp_variable = { t_1 = global.actor_template_base_fire_resistance^e_1 }
    multiply_temp_variable = { t_1 = e_1_w }
    set_temp_variable = { t_2 = global.actor_template_base_fire_resistance^e_2 }
    multiply_temp_variable = { t_2 = e_2_w }
    set_variable = { actor_stats_fire_resistance^a = t_1 }
    add_to_variable = { actor_stats_fire_resistance^a = t_2 }
}
else = {
    set_variable = { actor_stats_fire_resistance^a = global.actor_template_base_fire_resistance^e_1 }
}
if = {
    limit = { check_variable = { global.actor_template_base_cold_resistance^e_2 > 0 } }

    set_temp_variable = { t_1 = global.actor_template_base_cold_resistance^e_1 }
    multiply_temp_variable = { t_1 = e_1_w }
    set_temp_variable = { t_2 = global.actor_template_base_cold_resistance^e_2 }
    multiply_temp_variable = { t_2 = e_2_w }
    set_variable = { actor_stats_cold_resistance^a = t_1 }
    add_to_variable = { actor_stats_cold_resistance^a = t_2 }
}
else = {
    set_variable = { actor_stats_cold_resistance^a = global.actor_template_base_cold_resistance^e_1 }
}
if = {
    limit = { check_variable = { global.actor_template_base_lighting_resistance^e_2 > 0 } }

    set_temp_variable = { t_1 = global.actor_template_base_lighting_resistance^e_1 }
    multiply_temp_variable = { t_1 = e_1_w }
    set_temp_variable = { t_2 = global.actor_template_base_lighting_resistance^e_2 }
    multiply_temp_variable = { t_2 = e_2_w }
    set_variable = { actor_stats_lighting_resistance^a = t_1 }
    add_to_variable = { actor_stats_lighting_resistance^a = t_2 }
}
else = {
    set_variable = { actor_stats_lighting_resistance^a = global.actor_template_base_lighting_resistance^e_1 }
}
if = {
    limit = { check_variable = { global.actor_template_base_magic_resistance^e_2 > 0 } }

    set_temp_variable = { t_1 = global.actor_template_base_magic_resistance^e_1 }
    multiply_temp_variable = { t_1 = e_1_w }
    set_temp_variable = { t_2 = global.actor_template_base_magic_resistance^e_2 }
    multiply_temp_variable = { t_2 = e_2_w }
    set_variable = { actor_stats_magic_resistance^a = t_1 }
    add_to_variable = { actor_stats_magic_resistance^a = t_2 }
}
else = {
    set_variable = { actor_stats_magic_resistance^a = global.actor_template_base_magic_resistance^e_1 }
}
if = {
    limit = { check_variable = { global.actor_template_base_poison_resistance^e_2 > 0 } }

    set_temp_variable = { t_1 = global.actor_template_base_poison_resistance^e_1 }
    multiply_temp_variable = { t_1 = e_1_w }
    set_temp_variable = { t_2 = global.actor_template_base_poison_resistance^e_2 }
    multiply_temp_variable = { t_2 = e_2_w }
    set_variable = { actor_stats_poison_resistance^a = t_1 }
    add_to_variable = { actor_stats_poison_resistance^a = t_2 }
}
else = {
    set_variable = { actor_stats_poison_resistance^a = global.actor_template_base_poison_resistance^e_1 }
}
if = {
    limit = { check_variable = { global.actor_template_base_fire_damage_min^e_2 > 0 } }

    set_temp_variable = { t_1 = global.actor_template_base_fire_damage_min^e_1 }
    multiply_temp_variable = { t_1 = e_1_w }
    set_temp_variable = { t_2 = global.actor_template_base_fire_damage_min^e_2 }
    multiply_temp_variable = { t_2 = e_2_w }
    set_variable = { actor_stats_fire_damage_min^a = t_1 }
    add_to_variable = { actor_stats_fire_damage_min^a = t_2 }
}
else = {
    set_variable = { actor_stats_fire_damage_min^a = global.actor_template_base_fire_damage_min^e_1 }
}
if = {
    limit = { check_variable = { global.actor_template_base_fire_damage_max^e_2 > 0 } }

    set_temp_variable = { t_1 = global.actor_template_base_fire_damage_max^e_1 }
    multiply_temp_variable = { t_1 = e_1_w }
    set_temp_variable = { t_2 = global.actor_template_base_fire_damage_max^e_2 }
    multiply_temp_variable = { t_2 = e_2_w }
    set_variable = { actor_stats_fire_damage_max^a = t_1 }
    add_to_variable = { actor_stats_fire_damage_max^a = t_2 }
}
else = {
    set_variable = { actor_stats_fire_damage_max^a = global.actor_template_base_fire_damage_max^e_1 }
}
if = {
    limit = { check_variable = { global.actor_template_base_cold_damage_min^e_2 > 0 } }

    set_temp_variable = { t_1 = global.actor_template_base_cold_damage_min^e_1 }
    multiply_temp_variable = { t_1 = e_1_w }
    set_temp_variable = { t_2 = global.actor_template_base_cold_damage_min^e_2 }
    multiply_temp_variable = { t_2 = e_2_w }
    set_variable = { actor_stats_cold_damage_min^a = t_1 }
    add_to_variable = { actor_stats_cold_damage_min^a = t_2 }
}
else = {
    set_variable = { actor_stats_cold_damage_min^a = global.actor_template_base_cold_damage_min^e_1 }
}
if = {
    limit = { check_variable = { global.actor_template_base_cold_damage_max^e_2 > 0 } }

    set_temp_variable = { t_1 = global.actor_template_base_cold_damage_max^e_1 }
    multiply_temp_variable = { t_1 = e_1_w }
    set_temp_variable = { t_2 = global.actor_template_base_cold_damage_max^e_2 }
    multiply_temp_variable = { t_2 = e_2_w }
    set_variable = { actor_stats_cold_damage_max^a = t_1 }
    add_to_variable = { actor_stats_cold_damage_max^a = t_2 }
}
else = {
    set_variable = { actor_stats_cold_damage_max^a = global.actor_template_base_cold_damage_max^e_1 }
}
if = {
    limit = { check_variable = { global.actor_template_base_lighting_damage_min^e_2 > 0 } }

    set_temp_variable = { t_1 = global.actor_template_base_lighting_damage_min^e_1 }
    multiply_temp_variable = { t_1 = e_1_w }
    set_temp_variable = { t_2 = global.actor_template_base_lighting_damage_min^e_2 }
    multiply_temp_variable = { t_2 = e_2_w }
    set_variable = { actor_stats_lighting_damage_min^a = t_1 }
    add_to_variable = { actor_stats_lighting_damage_min^a = t_2 }
}
else = {
    set_variable = { actor_stats_lighting_damage_min^a = global.actor_template_base_lighting_damage_min^e_1 }
}
if = {
    limit = { check_variable = { global.actor_template_base_lighting_damage_max^e_2 > 0 } }

    set_temp_variable = { t_1 = global.actor_template_base_lighting_damage_max^e_1 }
    multiply_temp_variable = { t_1 = e_1_w }
    set_temp_variable = { t_2 = global.actor_template_base_lighting_damage_max^e_2 }
    multiply_temp_variable = { t_2 = e_2_w }
    set_variable = { actor_stats_lighting_damage_max^a = t_1 }
    add_to_variable = { actor_stats_lighting_damage_max^a = t_2 }
}
else = {
    set_variable = { actor_stats_lighting_damage_max^a = global.actor_template_base_lighting_damage_max^e_1 }
}
if = {
    limit = { check_variable = { global.actor_template_base_life_steal_mult^e_2 > 0 } }

    set_temp_variable = { t_1 = global.actor_template_base_life_steal_mult^e_1 }
    multiply_temp_variable = { t_1 = e_1_w }
    set_temp_variable = { t_2 = global.actor_template_base_life_steal_mult^e_2 }
    multiply_temp_variable = { t_2 = e_2_w }
    set_variable = { actor_stats_life_steal_mult^a = t_1 }
    add_to_variable = { actor_stats_life_steal_mult^a = t_2 }
}
else = {
    set_variable = { actor_stats_life_steal_mult^a = global.actor_template_base_life_steal_mult^e_1 }
}
if = {
    limit = { check_variable = { global.actor_template_base_life_steal_chance^e_2 > 0 } }

    set_temp_variable = { t_1 = global.actor_template_base_life_steal_chance^e_1 }
    multiply_temp_variable = { t_1 = e_1_w }
    set_temp_variable = { t_2 = global.actor_template_base_life_steal_chance^e_2 }
    multiply_temp_variable = { t_2 = e_2_w }
    set_variable = { actor_stats_life_steal_chance^a = t_1 }
    add_to_variable = { actor_stats_life_steal_chance^a = t_2 }
}
else = {
    set_variable = { actor_stats_life_steal_chance^a = global.actor_template_base_life_steal_chance^e_1 }
}
if = {
    limit = { check_variable = { global.actor_template_base_mana_steal_mult^e_2 > 0 } }

    set_temp_variable = { t_1 = global.actor_template_base_mana_steal_mult^e_1 }
    multiply_temp_variable = { t_1 = e_1_w }
    set_temp_variable = { t_2 = global.actor_template_base_mana_steal_mult^e_2 }
    multiply_temp_variable = { t_2 = e_2_w }
    set_variable = { actor_stats_mana_steal_mult^a = t_1 }
    add_to_variable = { actor_stats_mana_steal_mult^a = t_2 }
}
else = {
    set_variable = { actor_stats_mana_steal_mult^a = global.actor_template_base_mana_steal_mult^e_1 }
}
if = {
    limit = { check_variable = { global.actor_template_base_mana_steal_chance^e_2 > 0 } }

    set_temp_variable = { t_1 = global.actor_template_base_mana_steal_chance^e_1 }
    multiply_temp_variable = { t_1 = e_1_w }
    set_temp_variable = { t_2 = global.actor_template_base_mana_steal_chance^e_2 }
    multiply_temp_variable = { t_2 = e_2_w }
    set_variable = { actor_stats_mana_steal_chance^a = t_1 }
    add_to_variable = { actor_stats_mana_steal_chance^a = t_2 }
}
else = {
    set_variable = { actor_stats_mana_steal_chance^a = global.actor_template_base_mana_steal_chance^e_1 }
}
if = {
    limit = { check_variable = { global.actor_template_base_pure_damage_min^e_2 > 0 } }

    set_temp_variable = { t_1 = global.actor_template_base_pure_damage_min^e_1 }
    multiply_temp_variable = { t_1 = e_1_w }
    set_temp_variable = { t_2 = global.actor_template_base_pure_damage_min^e_2 }
    multiply_temp_variable = { t_2 = e_2_w }
    set_variable = { actor_stats_pure_damage_min^a = t_1 }
    add_to_variable = { actor_stats_pure_damage_min^a = t_2 }
}
else = {
    set_variable = { actor_stats_pure_damage_min^a = global.actor_template_base_pure_damage_min^e_1 }
}
if = {
    limit = { check_variable = { global.actor_template_base_pure_damage_max^e_2 > 0 } }

    set_temp_variable = { t_1 = global.actor_template_base_pure_damage_max^e_1 }
    multiply_temp_variable = { t_1 = e_1_w }
    set_temp_variable = { t_2 = global.actor_template_base_pure_damage_max^e_2 }
    multiply_temp_variable = { t_2 = e_2_w }
    set_variable = { actor_stats_pure_damage_max^a = t_1 }
    add_to_variable = { actor_stats_pure_damage_max^a = t_2 }
}
else = {
    set_variable = { actor_stats_pure_damage_max^a = global.actor_template_base_pure_damage_max^e_1 }
}
if = {
    limit = { check_variable = { global.actor_template_base_critical_mult^e_2 > 0 } }

    set_temp_variable = { t_1 = global.actor_template_base_critical_mult^e_1 }
    multiply_temp_variable = { t_1 = e_1_w }
    set_temp_variable = { t_2 = global.actor_template_base_critical_mult^e_2 }
    multiply_temp_variable = { t_2 = e_2_w }
    set_variable = { actor_stats_critical_mult^a = t_1 }
    add_to_variable = { actor_stats_critical_mult^a = t_2 }
}
else = {
    set_variable = { actor_stats_critical_mult^a = global.actor_template_base_critical_mult^e_1 }
}
if = {
    limit = { check_variable = { global.actor_template_base_critical_chance^e_2 > 0 } }

    set_temp_variable = { t_1 = global.actor_template_base_critical_chance^e_1 }
    multiply_temp_variable = { t_1 = e_1_w }
    set_temp_variable = { t_2 = global.actor_template_base_critical_chance^e_2 }
    multiply_temp_variable = { t_2 = e_2_w }
    set_variable = { actor_stats_critical_chance^a = t_1 }
    add_to_variable = { actor_stats_critical_chance^a = t_2 }
}
else = {
    set_variable = { actor_stats_critical_chance^a = global.actor_template_base_critical_chance^e_1 }
}
if = {
    limit = { check_variable = { global.actor_template_base_mana_regen^e_2 > 0 } }

    set_temp_variable = { t_1 = global.actor_template_base_mana_regen^e_1 }
    multiply_temp_variable = { t_1 = e_1_w }
    set_temp_variable = { t_2 = global.actor_template_base_mana_regen^e_2 }
    multiply_temp_variable = { t_2 = e_2_w }
    set_variable = { actor_stats_mana_regen^a = t_1 }
    add_to_variable = { actor_stats_mana_regen^a = t_2 }
}
else = {
    set_variable = { actor_stats_mana_regen^a = global.actor_template_base_mana_regen^e_1 }
}
if = {
    limit = { check_variable = { global.actor_template_base_life_regen^e_2 > 0 } }

    set_temp_variable = { t_1 = global.actor_template_base_life_regen^e_1 }
    multiply_temp_variable = { t_1 = e_1_w }
    set_temp_variable = { t_2 = global.actor_template_base_life_regen^e_2 }
    multiply_temp_variable = { t_2 = e_2_w }
    set_variable = { actor_stats_life_regen^a = t_1 }
    add_to_variable = { actor_stats_life_regen^a = t_2 }
}
else = {
    set_variable = { actor_stats_life_regen^a = global.actor_template_base_life_regen^e_1 }
}
}
set_actor_to_template_weighted_post = {
round_variable = actor_stats_actor_level^a
round_variable = actor_stats_max_life^a
round_variable = actor_stats_max_mana^a
round_variable = actor_stats_damage_min^a
round_variable = actor_stats_damage_max^a
round_variable = actor_stats_fire_damage_min^a
round_variable = actor_stats_fire_damage_max^a
round_variable = actor_stats_cold_damage_min^a
round_variable = actor_stats_cold_damage_max^a
round_variable = actor_stats_lighting_damage_min^a
round_variable = actor_stats_lighting_damage_max^a
round_variable = actor_stats_pure_damage_min^a
round_variable = actor_stats_pure_damage_max^a
}
