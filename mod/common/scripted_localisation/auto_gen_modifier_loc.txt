defined_text = { 
    name = mod_experience_gain_army
    text = {
        localization_key = MODIFIER_XP_GAIN_ARMY
    }
}

defined_text = { 
    name = mod_experience_gain_navy
    text = {
        localization_key = MODIFIER_XP_GAIN_NAVY
    }
}

defined_text = { 
    name = mod_experience_gain_air
    text = {
        localization_key = MODIFIER_XP_GAIN_AIR
    }
}

defined_text = { 
    name = mod_resistance_target
    text = {
        localization_key = MODIFIER_RESISTANCE_TARGET
    }
}

defined_text = { 
    name = mod_compliance_gain
    text = {
        localization_key = MODIFIER_COMPLIANCE_GAIN_ADD
    }
}

defined_text = { 
    name = mod_resistance_damage_to_garrison
    text = {
        localization_key = MODIFIER_RESISTANCE_DAMAGE_TO_GARRISONS
    }
}

defined_text = { 
    name = mod_industrial_capacity_factory
    text = {
        localization_key = MODIFIER_INDUSTRIAL_CAPACITY_FACTOR
    }
}

defined_text = { 
    name = mod_industrial_capacity_dockyard
    text = {
        localization_key = MODIFIER_INDUSTRIAL_CAPACITY_DOCKYARD_FACTOR
    }
}

defined_text = { 
    name = mod_industry_repair_factor
    text = {
        localization_key = MODIFIER_INDUSTRY_REPAIR_FACTOR
    }
}

defined_text = { 
    name = mod_industry_free_repair_factor
    text = {
        localization_key = MODIFIER_INDUSTRY_FREE_REPAIR_FACTOR
    }
}

defined_text = { 
    name = mod_repair_speed_factor
    text = {
        localization_key = MODIFIER_REPAIR_SPEED_FACTOR
    }
}

defined_text = { 
    name = mod_army_armor_attack_factor
    text = {
        localization_key = MODIFIERS_ARMY_ARMOR_ATTACK_FACTOR
    }
}

defined_text = { 
    name = mod_army_armor_defence_factor
    text = {
        localization_key = MODIFIERS_ARMY_ARMOR_DEFENCE_FACTOR
    }
}

defined_text = { 
    name = mod_army_artillery_attack_factor
    text = {
        localization_key = MODIFIERS_ARMY_ARTILLERY_ATTACK_FACTOR
    }
}

defined_text = { 
    name = mod_army_artillery_defence_factor
    text = {
        localization_key = MODIFIERS_ARMY_ARTILLERY_DEFENCE_FACTOR
    }
}

defined_text = { 
    name = mod_army_infantry_attack_factor
    text = {
        localization_key = MODIFIERS_ARMY_INFANTRY_ATTACK_FACTOR
    }
}

defined_text = { 
    name = mod_army_infantry_defence_factor
    text = {
        localization_key = MODIFIERS_ARMY_INFANTRY_DEFENCE_FACTOR
    }
}

defined_text = { 
    name = mod_special_forces_attack_factor
    text = {
        localization_key = MODIFIER_SPECIAL_FORCES_ATTACK_FACTOR
    }
}

defined_text = { 
    name = mod_special_forces_defence_factor
    text = {
        localization_key = MODIFIER_SPECIAL_FORCES_DEFENCE_FACTOR
    }
}

defined_text = { 
    name = mod_cavalry_attack_factor
    text = {
        localization_key = MODIFIER_CAVALRY_ATTACK_FACTOR
    }
}

defined_text = { 
    name = mod_cavalry_defence_factor
    text = {
        localization_key = MODIFIER_CAVALRY_DEFENCE_FACTOR
    }
}

defined_text = { 
    name = mod_motorized_attack_factor
    text = {
        localization_key = MODIFIER_MOTORIZED_ATTACK_FACTOR
    }
}

defined_text = { 
    name = mod_motorized_defence_factor
    text = {
        localization_key = MODIFIER_MOTORIZED_DEFENCE_FACTOR
    }
}

defined_text = { 
    name = mod_mechanized_attack_factor
    text = {
        localization_key = MODIFIER_MECHANIZED_ATTACK_FACTOR
    }
}

defined_text = { 
    name = mod_mechanized_defence_factor
    text = {
        localization_key = MODIFIER_MECHANIZED_DEFENCE_FACTOR
    }
}

defined_text = { 
    name = mod_research_speed_factor
    text = {
        localization_key = MODIFIER_RESEARCH_SPEED_FACTOR
    }
}

defined_text = { 
    name = mod_training_time_army_factor
    text = {
        localization_key = MODIFIER_TRAINING_TIME_ARMY_FACTOR
    }
}

defined_text = { 
    name = mod_stability_factor
    text = {
        localization_key = MODIFIER_STABILITY_FACTOR
    }
}

defined_text = { 
    name = mod_war_support_factor
    text = {
        localization_key = MODIFIER_WAR_SUPPORT_FACTOR
    }
}

defined_text = { 
    name = mod_production_speed_buildings_factor
    text = {
        localization_key = MODIFIER_PRODUCTION_SPEED_BUILDINGS_FACTOR
    }
}

defined_text = { 
    name = mod_production_speed_bunker_factor
    text = {
        localization_key = modifier_production_speed_bunker_factor
    }
}

defined_text = { 
    name = mod_production_speed_coastal_bunker_factor
    text = {
        localization_key = modifier_production_speed_coastal_bunker_factor
    }
}

defined_text = { 
    name = mod_production_speed_anti_air_building_factor
    text = {
        localization_key = modifier_production_speed_anti_air_building_factor
    }
}

defined_text = { 
    name = mod_production_speed_supply_node_factor
    text = {
        localization_key = modifier_production_speed_supply_node_factor
    }
}

defined_text = { 
    name = mod_production_speed_infrastructure_factor
    text = {
        localization_key = modifier_production_speed_infrastructure_factor
    }
}

defined_text = { 
    name = mod_production_speed_rail_way_factor
    text = {
        localization_key = modifier_production_speed_rail_way_factor
    }
}

defined_text = { 
    name = mod_production_speed_air_base_factor
    text = {
        localization_key = modifier_production_speed_air_base_factor
    }
}

defined_text = { 
    name = mod_production_speed_naval_base_factor
    text = {
        localization_key = modifier_production_speed_naval_base_factor
    }
}

defined_text = { 
    name = mod_production_speed_arms_factory_factor
    text = {
        localization_key = modifier_production_speed_arms_factory_factor
    }
}

defined_text = { 
    name = mod_production_speed_industrial_complex_factor
    text = {
        localization_key = modifier_production_speed_industrial_complex_factor
    }
}

defined_text = { 
    name = mod_army_attack_factor
    text = {
        localization_key = MODIFIERS_ARMY_ATTACK_FACTOR
    }
}

defined_text = { 
    name = mod_army_speed_factor
    text = {
        localization_key = MODIFIER_ARMY_SPEED_FACTOR
    }
}

defined_text = { 
    name = mod_army_defence_factor
    text = {
        localization_key = MODIFIERS_ARMY_DEFENCE_FACTOR
    }
}

defined_text = { 
    name = mod_breakthrough_factor
    text = {
        localization_key = MODIFIER_BREAKTHROUGH
    }
}

defined_text = { 
    name = mod_army_core_attack_factor
    text = {
        localization_key = MODIFIERS_ARMY_CORE_ATTACK_FACTOR
    }
}

defined_text = { 
    name = mod_army_core_defence_factor
    text = {
        localization_key = MODIFIERS_ARMY_CORE_DEFENCE_FACTOR
    }
}

defined_text = { 
    name = mod_army_org_factor
    text = {
        localization_key = MODIFIER_ARMY_ORG_FACTOR
    }
}

defined_text = { 
    name = mod_army_org_regain
    text = {
        localization_key = MODIFIER_ARMY_ORG_REGAIN
    }
}

defined_text = { 
    name = mod_army_morale_factor
    text = {
        localization_key = MODIFIER_ARMY_MORALE_FACTOR
    }
}

defined_text = { 
    name = mod_planning_speed
    text = {
        localization_key = MODIFIER_PLANNING_SPEED
    }
}

defined_text = { 
    name = mod_max_planning
    text = {
        localization_key = MODIFIER_MAX_PLANNING
    }
}

defined_text = { 
    name = mod_dig_in_speed_factor
    text = {
        localization_key = MODIFIER_DIG_IN_SPEED_FACTOR
    }
}

defined_text = { 
    name = mod_max_dig_in_factor
    text = {
        localization_key = MODIFIER_MAX_DIG_IN_FACTOR
    }
}

defined_text = { 
    name = mod_max_command_power
    text = {
        localization_key = MODIFIER_MAX_COMMAND_POWER
    }
}

defined_text = { 
    name = mod_command_power_gain_mult
    text = {
        localization_key = MODIFIER_COMMAND_POWER_GAIN_MULT
    }
}

defined_text = { 
    name = mod_weekly_manpower
    text = {
        localization_key = MODIFIER_WEEKLY_MANPOWER
    }
}

defined_text = { 
    name = mod_supply_node_range
    text = {
        localization_key = MODIFIER_SUPPLY_NODE_RANGE
    }
}

defined_text = { 
    name = mod_out_of_supply_factor
    text = {
        localization_key = MODIFIER_OUT_OF_SUPPLY_FACTOR
    }
}

defined_text = { 
    name = mod_supply_consumption_factor
    text = {
        localization_key = MODIFIER_SUPPLY_CONSUMPTION_FACTOR
    }
}

defined_text = { 
    name = mod_acclimatization_cold_climate_gain_factor
    text = {
        localization_key = MODIFIER_ACCLIMATIZATION_COLD_CLIMATE_GAIN_FACTOR
    }
}

defined_text = { 
    name = mod_acclimatization_hot_climate_gain_factor
    text = {
        localization_key = MODIFIER_ACCLIMATIZATION_HOT_CLIMATE_GAIN_FACTOR
    }
}

defined_text = { 
    name = mod_naval_damage_factor
    text = {
        localization_key = MODIFIER_NAVAL_DAMAGE_FACTOR
    }
}

defined_text = { 
    name = mod_naval_defense_factor
    text = {
        localization_key = MODIFIER_NAVAL_DEFENSE_FACTOR
    }
}

defined_text = { 
    name = mod_naval_hit_chance
    text = {
        localization_key = MODIFIER_NAVAL_HIT_CHANCE
    }
}

defined_text = { 
    name = mod_naval_invasion_capacity
    text = {
        localization_key = MODIFIER_NAVAL_INVASION_CAPACITY
    }
}

defined_text = { 
    name = mod_naval_invasion_penalty
    text = {
        localization_key = MODIFIER_NAVAL_INVASION_PENALTY
    }
}

defined_text = { 
    name = mod_naval_invasion_prep_speed
    text = {
        localization_key = MODIFIER_NAVAL_INVASION_PLANNING_SPEED
    }
}

defined_text = { 
    name = mod_air_attack_factor
    text = {
        localization_key = MODIFIER_AIR_ATTACK_FACTOR
    }
}

defined_text = { 
    name = mod_air_defence_factor
    text = {
        localization_key = MODIFIER_AIR_DEFENCE_FACTOR
    }
}

defined_text = { 
    name = mod_air_agility_factor
    text = {
        localization_key = MODIFIER_AIR_AGILITY_FACTOR
    }
}

defined_text = { 
    name = mod_paratrooper_count_per_plane
    text = {
        localization_key = MODIFIER_UNIT_SIZE_FACTOR_FOR_PARADROP
    }
}

defined_text = { 
    name = mod_paradrop_organization_factor
    text = {
        localization_key = MODIFIER_PARADROP_ORGANIZATION_FACTOR
    }
}

defined_text = { 
    name = mod_paratrooper_aa_defense
    text = {
        localization_key = MODIFIER_PARATROOPER_DEFENSE
    }
}

