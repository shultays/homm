scripted_gui = {
    homm_battle_result_window = {
        context_type = player_context
        
        window_name = "homm_battle_result_window"
        
        visible = {
            check_variable = { battle_result_visible = 1 }
        }
        
        triggers = {
            selected_bg_visible = {
                check_variable = { ROOT.selected_combat_loot^i = 1 }
            }
            accept_button_click_enabled = {
                can_take_all_items = yes
                can_take_all_captured = yes
            }
            selected_charcater_bg_visible = {
                check_variable = { e = 2 }
            }
        }
        
        effects = {
            accept_button_click = {
                set_variable = { battle_result_visible = 0 }
                
                get_selected_loot = yes
                get_selected_monsters = yes
                
                clear_post_combat = yes
            }
            decline_button_click = {
                set_variable = { battle_result_visible = 0 }
                clear_post_combat = yes
            }
            
            item_icon_click = {
                if = { 
                    limit = { check_variable = { ROOT.selected_combat_loot^i = 1 } }
                    set_variable = { ROOT.selected_combat_loot^i = 0 }
                }
                else = {
                    set_variable = { ROOT.selected_combat_loot^i = 1 }
                }
            }
            char_bg_click = {
                if = { 
                    limit = { check_variable = { actor_type^a = 2 } }
                    set_variable = { actor_type^a = 3 }
                    else = {
                        if = { 
                            limit = { check_variable = { actor_type^a = 3 } }
                            set_variable = { actor_type^a = 2 }
                        }
                    }
                }
            }
        }
        
        dynamic_lists = {
            inventory_grid = {
                array = combat_loot
                entry_container = "homm_item_entry"
            }

            dead_characters_grid = {
                array = actor_type
                entry_container = "[homm_combat_result_dead_actors]"
                value = e
                index = a
            }
            captured_characters_grid = {
                array = actor_type
                entry_container = "[homm_combat_result_captured_actors]"
                value = e
                index = a
            }
            #combat_history = {
            #    array = combat_history_line
            #    entry_container = "homm_combat_history_entry"
            #}
        }
        
        properties = {
            char_bg = {
                image = "[actor_icon_type]"
                frame = actor_icon^a
            }
            item_icon = {
                image = "[This.homm_item_texture]"
                frame = global.icon_frames^v
            }
            item_icon_small = {
                image = "[This.homm_item_small_texture]"
                frame = global.icon_frames^v
            }
            quality_bg = {
                frame = global.item_qualities^v
            }
        }
    }
}

