(this document is still incomplete)

Majority of game content is generated using these two google sheets

[Item Data](https://docs.google.com/spreadsheets/d/1HZzHnN9D3HiAVmRvcOCnL1PT9_Kkzk1jb_Rlc4KAFpQ)
Used for generating items and related stuff

[Artifact Data](https://docs.google.com/spreadsheets/d/17iCIumGvZVmSZhU8vEGiHCyAqyqKfmzDdKsr-y6ffkk/edit#gid=1093583018)
Used for generating artifacts

[Actor Data](https://docs.google.com/spreadsheets/d/1xSH6BHPEt9B2_P-yAJcvPMddPNpjCC228qz8xLN1LHQ)
Used for generating actors and related stuff

# Items

<details>
  <summary>Click to Expand</summary>

Item data are stored in global scope. For example global.item_types stores the type (weapon, armor, ring etc) of items while global.item_stat_1 stores the first item stat etc

All other places refers items using an item index (often stored as temp variable "n"). For example a country has an inventory array that stores indices of items in its inventory/equipment. Transferring one item from a country to another is just transfering its index

## Item types

<details>
  <summary>Click to Expand</summary>

Each item has a type and a sub type. 

An item type represents which slot the item can be wielded. For example an item type can be "weapon" or a "shield". Item types are stored in global.item_types

Current item types
10: weapon (this is actually sword. weapons are a bit special and represented by multiple item types. Item type for "spear" is 110 for example. One day I will try to refactor this)
20: armor
30: helm
40: cape
50: shield
60: ring
70: amulet
80: boots
90: gloves

An item subtype is a sub category for item. For example for item type "weapon", item sub types are "sword", "axe" etc

Each subtype has a unique id. For example "leather armor" is 21, while "plate mail" is 25. An item subtype is its type plus some number (ie type "armor" is 20 while sub type "plate mail" is 20+5). The game stores this sub type addition (5 for "plate mail") in global.item_indices

</details>

## Item stats

<details>
  <summary>Click to Expand</summary>

(Currently) An item can have at most 5 (and plus another unique) stats. Stats are stored as global.item_stat_N, global.item_stat_N_a, ..._b and .._c. global.item_stat_1 represents the type of the stat while _a, _b and _c represents the deteails. For example if global.item_stat_1 is 7, item gives fire resistance. And the amount of fire resistance it gives is stored in global.item_stat_1_a. For fire resistance, the value stored in _b and _c is irrelevant. 

For weapons global.item_stat_1 is always 0, which is the attack stat of the weapon while _a and _b values represents min and max damage and _c is the attack speed.

For most armors global.item_stat_1 is armor. And _a is armor value

Item stats can be found at:
https://docs.google.com/spreadsheets/d/1HZzHnN9D3HiAVmRvcOCnL1PT9_Kkzk1jb_Rlc4KAFpQ/edit#gid=0

### Stat strings

A randomly added stat can add a prefix or a postfix to its name. These prefixes & postfixes are stored in global.item_name_pre and global.item_name_post. An item name is generated using its type and its prefix and post fix.
These prefixes are auto generated using this sheet:
https://docs.google.com/spreadsheets/d/1HZzHnN9D3HiAVmRvcOCnL1PT9_Kkzk1jb_Rlc4KAFpQ/edit#gid=1114035392

Each stat is also represented in the item tooltips and these tooltips are auto generated using this sheet:
https://docs.google.com/spreadsheets/d/1HZzHnN9D3HiAVmRvcOCnL1PT9_Kkzk1jb_Rlc4KAFpQ/edit#gid=157306750

### Random item generation

Each different item type has different chance to generate different stats. For example weapons are more likely to add fire/lighting etc damage while armors are more likely to generate resistances etc. The game actually uses item sub types for this randomization. For example a claymore has higher chance to add str while a katana has a higher chance to have critical strike.

Which items types can generate which stats are stored in individual item type sheets. For sword we have this:
https://docs.google.com/spreadsheets/d/1HZzHnN9D3HiAVmRvcOCnL1PT9_Kkzk1jb_Rlc4KAFpQ/edit#gid=1993173612

todo: implementation details

</details>

## Unique items

<details>
  <summary>Click to Expand</summary>

Unique items are items with unique names (and often icons) that have predefined stats which predefined values and auto generated using this sheet:
https://docs.google.com/spreadsheets/d/1HZzHnN9D3HiAVmRvcOCnL1PT9_Kkzk1jb_Rlc4KAFpQ/edit#gid=360565350

Unique items have unique names (stored in global.item_unique_name) and can have a unique stat (stored in global.item_u_stat and has stats stored as global.item_u_stat_a ..._b ..._c)

todo: implementation details

#### Leveling items

Unique items are often marked as "leveling". This means that they will upgrade as the hero levels up and will get better stats

Leveling items have often have negative stat ids

todo: implementation details

</details>


</details>

# Actors

<details>
  <summary>Click to Expand</summary>

Actors can be either "heroes" or "monsters"

Each country has one hero and can have additional monsters. States can only have monsters.

Actor data are stored in country/state scope (unlike items, whose stats are stored in global scope). Similar to items, actors have different arrays to store different stat types. For example "actor_level" stores level of actors while actor_life stores current life)

## Monsters

## Hero

</details>
